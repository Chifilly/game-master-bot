import type { IStorage } from './interface';

export const defaultData: IStorage = {
	version: 1,
	guilds:  new Map(),
};
