
import { join } from 'path';
import { promises as fs } from 'fs';

import * as root from 'app-root-path';
import type
{
	Guild,
	GuildMember,
	Message,
	Role,
	User,
} from 'discord.js';
import { DateTime } from 'luxon';
import { customAlphabet } from 'nanoid/non-secure';

import { logger, label } from '../logger';
import { isNullOrUndefined } from '../../helpers/utilities';
import type
{
	ISpecialJSONObject,
	IStorage,
	IStorageCampaign,
	IStorageCampaignBase,
	IStorageChannels,
	IStorageGuild,
	IStorageMessage,
	IStorageNext,
	IStorageReminderTime,
	TStorageCampaigns,
	TStorageLeaders,
	TStorageManagers,
	TStoragePoll,
} from './interface';
import { parseReminderString } from '../discord/helpers';
import
{
	isSpecialJSONObject,
	isSpecialJSONObjectDateTime,
	isSpecialJSONObjectMap,
	isSpecialJSONObjectSet,
} from './guards';
import { constants } from '../../helpers/constants';

export type
{
	IStorage,
	IStorageCampaign,
	IStorageCampaignBase as IStorageCampaignBasic,
	IStorageChannels,
	IStorageGuild,
	IStorageMessage,
	IStorageNext,
	IStorageReminderTime,
	TStorageCampaigns,
	TStorageLeaders,
	TStorageManagers,
	TStoragePoll,
} from './interface';

export enum CampaignStatus
{
	SET,
	VOTING,
	NONE,
}

const nanoid = customAlphabet('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', constants.numbers.NANOID_LENGTH);

// I need to do this, because JSON.stringify - even with a custom replacer - still calls .toJSON on items
// and it causes luxon objects to get turned into ISO strings before I even get a chance to put them in
// the format I want for storing in the database
Reflect.deleteProperty(DateTime.prototype, 'toJSON');

class StorageClass
{
	private readonly _filename: string = 'data';

	private readonly _fileExtension: string = 'json';

	private readonly _version: number = 2;

	public readonly defaultData: IStorage = {
		version: this._version,
		guilds: new Map<string, IStorageGuild>(),
	};

	private readonly _stringifyIndentSize: number = 4;

	private _data: IStorage = this.defaultData;

	private _saving: boolean = false;

	public async init(): Promise<void>
	{
		logger.info('Initialising database', label.DATABASE);

		this._load()
			.then(async (data) =>
			{
				try
				{
					this._data = JSON.parse(data, this._stringifyReviver);
				}
				catch(e: unknown)
				{
					if(e instanceof Error)
					{
						if(e.name === 'SyntaxError')
						{
							logger.error('Database file is malformed, so it will be recreated', label.DATABASE);

							this._data = { ...this.defaultData };
						}
						else
						{
							logger.error(`Failed to parse data from database: ${e.message}`, label.DATABASE);

							throw new Error();
						}
					}
					else
					{
						logger.error(`Unknown error parsing the database: ${e}`, label.DATABASE);

						throw new Error();
					}
				}

				let newData = { ...this._data };

				try
				{
					let isMigrating = newData.version !== this._version;

					if(isMigrating)
					{
						logger.info('Migrating database to a newer version', label.DATABASE);
					}

					while(isMigrating)
					{
						let file = null;

						try
						{
							file = await import(`./migrations/${newData.version}`);

							if(
								isNullOrUndefined(file.version) ||
								isNullOrUndefined(file.migrate) ||
								typeof file.version !== 'number' ||
								typeof file.migrate !== 'function'
							)
							{
								logger.error(`Migration file for version ${newData.version} invalid`);

								throw new Error();
							}

							newData = file.migrate({ ...newData });
						}
						catch(e)
						{
							logger.error(`Error migrating from database version ${newData.version}: ${e}`, label.DATABASE);

							throw new Error();
						}

						newData.version = file.version;

						logger.info(`Migrated database to version ${file.version}`, label.DATABASE);

						if(newData.version === this._version)
						{
							isMigrating = false;
						}
					}

					this._data = newData;

					await this.save();
				}
				catch(e)
				{
					logger.error(`Failed to migrate database to version ${this._version}: ${e}`, label.DATABASE);

					throw new Error();
				}

				logger.info('Successfully loaded data', label.DATABASE);
			})
			.catch((loadErr) =>
			{
				if(loadErr.code === 'ENOENT')
				{
					logger.info('No database found, attempting to create one', label.DATABASE);

					this.save()
						.then(() =>
						{
							logger.info('Successfully created data file', label.DATABASE);
						})
						.catch((saveErr) =>
						{
							logger.error(`Failed to create a new database: ${saveErr}`);

							throw new Error();
						});
				}
				else
				{
					logger.error(`Failed to load database: ${loadErr.message}`);

					throw new Error();
				}
			});
	}

	private _stringifyReplacer(this: void, key: string, item: unknown): unknown
	{
		if(item instanceof Map)
		{
			const converted: ISpecialJSONObject = {
				$type: 'Map',
				$value: [...item],
			};

			return converted;
		}

		if(item instanceof Set)
		{
			const converted: ISpecialJSONObject = {
				$type: 'Set',
				$value: [...item],
			};

			return converted;
		}

		if(DateTime.isDateTime(item))
		{
			const converted: ISpecialJSONObject = {
				$type: 'DateTime',
				$value: item.toISO(),
			};

			return converted;
		}

		return item;
	}

	private _stringifyReviver(this: void, key: string, item: unknown): unknown
	{
		if(isSpecialJSONObject(item))
		{
			if(isSpecialJSONObjectMap(item))
			{
				return new Map(item.$value);
			}

			if(isSpecialJSONObjectSet(item))
			{
				return new Set(item.$value);
			}

			if(isSpecialJSONObjectDateTime(item))
			{
				return DateTime.fromISO(item.$value).toUTC();
			}
		}

		return item;
	}

	private _getFileLocation(temp: boolean = false): string
	{
		return join(root.toString(), `${this._filename}${temp ? `_${DateTime.utc().toMillis()}` : ''}.${this._fileExtension}`);
	}

	private async _load(): Promise<string>
	{
		return fs.readFile(this._getFileLocation(), { encoding: 'utf8', flag: 'r' });
	}

	public async save(): Promise<Error | undefined>
	{
		if(this._saving)
		{
			return;
		}

		this._saving = true;

		let data: string = '';

		try
		{
			const stringifyArgs: [IStorage, (key: string, value: unknown) => unknown, number?] = [this._data, this._stringifyReplacer];

			// Adds 4-space indentation to non-production data files for easier reading during development
			if(process.env.NODE_ENV === 'development')
			{
				stringifyArgs.push(this._stringifyIndentSize);
			}

			data = JSON.stringify(...stringifyArgs);
		}
		catch(e)
		{
			this._saving = false;

			logger.error(`Error saving the database: ${e}`, label.DATABASE);

			return new Error('Error saving the database');
		}

		const tempFileLocation = this._getFileLocation(true);
		const mainFileLocation = this._getFileLocation(false);

		try
		{
			await fs.writeFile(tempFileLocation, data, { encoding: 'utf8', flag: 'w' });
			await fs.rename(tempFileLocation, mainFileLocation);
		}
		catch(e)
		{
			logger.error(`Error saving the database: ${e}`, label.DATABASE);
		}
		finally
		{
			this._saving = false;
		}
	}

	public async initGuild(guild: Guild, save: boolean = true): Promise<unknown>
	{
		if(!isNullOrUndefined(this._getGuild(guild.id)))
		{
			return;
		}

		const guildId = guild.id;
		const guildData: IStorageGuild = {
			roles: {
				leaders: new Set(),
				managers: new Set(),
			},
			channels: {},
			reminders: {
				enabled: true,
				times: [
					{
						text: '1d',
						number: parseReminderString('1d').as('seconds'),
					},
					{
						text: '1h',
						number: parseReminderString('1h').as('seconds'),
					},
					{
						text: '5m',
						number: parseReminderString('5m').as('seconds'),
					},
				],
			},
			campaigns: new Map(),
		};

		this._data.guilds.set(guildId, guildData);

		if(save)
		{
			return this.save();
		}
	}

	private _getGuild(guildId: string): IStorageGuild | undefined
	{
		return this._data.guilds.get(guildId);
	}

	private _getCampaign(guildId: string, campaignId: string): IStorageCampaign | undefined
	{
		const campaigns = this.getCampaigns(guildId);

		if(!isNullOrUndefined(campaigns))
		{
			return campaigns.get(campaignId);
		}
	}

	public isUserCampaignLeader(guildId: string, campaignId: string, userId: string): boolean
	{
		const campaign = this._getCampaign(guildId, campaignId);

		return !isNullOrUndefined(campaign) && campaign.leaders.has(userId);
	}

	public isMemberManager(guildId: string, member: GuildMember): boolean
	{
		const managers = this.getManagerRoles(guildId);

		for(const [role] of member.roles.cache)
		{
			if(managers.has(role))
			{
				return true;
			}
		}

		return false;
	}

	public doesCampaignExist(guildId: string, campaignId: string): boolean
	{
		return !isNullOrUndefined(this._getCampaign(guildId, campaignId));
	}

	public async purgeGuild(guildId: string, save: boolean = true): Promise<Error | undefined>
	{
		const wasDeleted = this._data.guilds.delete(guildId);

		if(save && wasDeleted)
		{
			return this.save();
		}

		if(!wasDeleted)
		{
			return new Error(`Couldn't delete guild ${guildId} from the database`);
		}
	}

	public getGuildIds(): string[]
	{
		return [...this._data.guilds.keys()];
	}

	public getLeaderRoles(guildId: string): TStorageLeaders | undefined
	{
		return this._getGuild(guildId)?.roles.leaders;
	}

	public async addLeaderRole(guildId: string, role: Role, save: boolean = true): Promise<Error | undefined>
	{
		const leaderRoles = this.getLeaderRoles(guildId);

		if(!isNullOrUndefined(leaderRoles))
		{
			if(!leaderRoles.has(role.id))
			{
				this._getGuild(guildId)?.roles.leaders.add(role.id);

				if(save)
				{
					return this.save();
				}

				return;
			}

			return new Error(`${role} is already a leader`);
		}

		return new Error('Could not find the leader list in the database');
	}

	public async removeLeaderRole(guildId: string, role: Role, save: boolean = true): Promise<Error | undefined>
	{
		const leaderRoles = this.getLeaderRoles(guildId);

		const wasDeleted = leaderRoles?.delete(role.id);

		if(save && wasDeleted)
		{
			return this.save();
		}

		if(!wasDeleted)
		{
			return new Error(`${role} is not a leader`);
		}
	}

	public getManagerRoles(guildId: string): TStorageManagers
	{
		return this._getGuild(guildId)?.roles.managers || new Set();
	}

	public async addManagerRole(guildId: string, role: Role, save: boolean = true): Promise<Error | undefined>
	{
		const managerRoles = this.getManagerRoles(guildId);

		if(!isNullOrUndefined(managerRoles))
		{
			if(!managerRoles.has(role.id))
			{
				this._getGuild(guildId)?.roles.managers.add(role.id);

				if(save)
				{
					return this.save();
				}

				return;
			}

			return new Error(`${role} is already a manager`);
		}

		return new Error('Could not find the manager list in the database');
	}

	public async removeManagerRole(guildId: string, role: Role, save: boolean = true): Promise<Error | undefined>
	{
		const managerRoles = this.getManagerRoles(guildId);

		const wasDeleted = managerRoles?.delete(role.id);

		if(save && wasDeleted)
		{
			return this.save();
		}

		if(!wasDeleted)
		{
			return new Error(`${role} is not a manager`);
		}
	}

	public getStaticChannel(guildId: string, type: keyof IStorageChannels): string | undefined
	{
		return this._getGuild(guildId)?.channels[type];
	}

	public async setStaticChannel(guildId: string, type: keyof IStorageChannels, channel: string, save: boolean = true): Promise<Error | undefined>
	{
		const guild = this._getGuild(guildId);

		if(!isNullOrUndefined(guild))
		{
			guild.channels[type] = channel;

			if(save)
			{
				return this.save();
			}

			return;
		}

		return new Error('Could not find the guild in the database');
	}

	public getRemindersEnabled(guildId: string): boolean
	{
		return this._getGuild(guildId)?.reminders.enabled || true;
	}

	public async setRemindersEnabled(guildId: string, enabled: boolean, save: boolean = true): Promise<Error | undefined>
	{
		const guild = this._getGuild(guildId);

		if(!isNullOrUndefined(guild))
		{
			guild.reminders.enabled = enabled;

			if(save)
			{
				return this.save();
			}

			return;
		}

		return new Error('Could not find the guild in the database');
	}

	public getReminderTimes(guildId: string): IStorageReminderTime[] | undefined
	{
		return this._getGuild(guildId)?.reminders.times;
	}

	public async setReminderTimes(guildId: string, times: IStorageReminderTime[], save: boolean = true): Promise<Error | IStorageReminderTime[]>
	{
		const sortedTimes = times.sort((a, b) =>
		{
			return b.number - a.number;
		});

		const guild = this._getGuild(guildId);

		if(!isNullOrUndefined(guild))
		{
			guild.reminders.times = sortedTimes;

			if(save)
			{
				const saved = await this.save();

				if(saved instanceof Error)
				{
					return saved;
				}
			}

			return sortedTimes;
		}

		return new Error('Could not find the guild in the database');
	}

	public async createCampaign(guildId: string, leader: string, name: string): Promise<Error | string>
	{
		const guild = this._getGuild(guildId);

		if(!isNullOrUndefined(guild))
		{
			const guildData: IStorageCampaignBase = {
				name,
				leaders: new Set([leader]),
				status: CampaignStatus.NONE,
			};

			let id = nanoid();

			while(guild.campaigns.has(id))
			{
				id = nanoid();
			}

			guild.campaigns.set(id, guildData);

			const saved = await this.save();

			if(saved instanceof Error)
			{
				return saved;
			}

			return id;
		}

		return new Error('Could not find the guild in the database');
	}

	public async renameCampaign(guildId: string, campaignId: string, name: string, save: boolean = true): Promise<Error | undefined>
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(!isNullOrUndefined(campaign))
		{
			campaign.name = name;

			if(save)
			{
				return this.save();
			}

			return;
		}

		return new Error('Could not find the campaign in the database');
	}

	public async deleteCampaign(guildId: string, campaignId: string): Promise<Error | undefined>
	{
		const campaigns = this.getCampaigns(guildId);

		if(!isNullOrUndefined(campaigns))
		{
			const wasDeleted = campaigns.delete(campaignId);

			if(wasDeleted)
			{
				return this.save();
			}

			return new Error(`No campaign with id \`${campaignId}\` found`);
		}

		return new Error('Could not find the guild in the database');
	}

	public getCampaigns(guildId: string): TStorageCampaigns | undefined
	{
		return this._getGuild(guildId)?.campaigns;
	}

	public getCampaignInfo(guildId: string, campaignId: string): IStorageCampaignBase | undefined
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(!isNullOrUndefined(campaign))
		{
			return {
				name: campaign.name,
				leaders: campaign.leaders,
				status: campaign.status,
			};
		}
	}

	public async setCampaignName(guildId: string, campaignId: string, name: string, save: boolean = true): Promise<Error | undefined>
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(!isNullOrUndefined(campaign))
		{
			campaign.name = name;

			if(save)
			{
				return this.save();
			}

			return;
		}

		return new Error(`No campaign with id \`${campaignId}\` found`);
	}

	public async setCampaignStatus(guildId: string, campaignId: string, status: CampaignStatus, save: boolean = true): Promise<Error | undefined>
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(!isNullOrUndefined(campaign))
		{
			campaign.status = status;

			switch(status)
			{
				case CampaignStatus.NONE:
					Reflect.deleteProperty(campaign, 'poll');
					Reflect.deleteProperty(campaign, 'next');
					Reflect.deleteProperty(campaign, 'message');

					break;

				case CampaignStatus.SET:
					Reflect.deleteProperty(campaign, 'poll');

					break;

				case CampaignStatus.VOTING:
					Reflect.deleteProperty(campaign, 'next');

					break;

				default:
					return new Error('Received an invalid campaign status. This is most likely a bug');
			}

			if(save)
			{
				return this.save();
			}
		}

		return new Error(`No campaign with id \`${campaignId}\` found`);
	}

	public getCampaignMessage(guildId: string, campaignId: string): IStorageMessage | undefined
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(!isNullOrUndefined(campaign))
		{
			return campaign.message;
		}
	}

	public async setCampaignMessage(guildId: string, campaignId: string, message: Message, save: boolean = true): Promise<Message>
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(!isNullOrUndefined(campaign))
		{
			campaign.message = {
				id: message.id,
				channel: message.channel.id,
				url: message.url,
			};

			if(save)
			{
				await this.save();
			}
		}

		return message;
	}

	public async deleteCampaignMessage(guildId: string, campaignId: string, save: boolean = true): Promise<Error | undefined>
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(!isNullOrUndefined(campaign))
		{
			return this.setCampaignStatus(guildId, campaignId, CampaignStatus.NONE, save);
		}

		return new Error(`No campaign with id \`${campaignId}\` found`);
	}

	public getPoll(guildId: string, campaignId: string): TStoragePoll | undefined
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(!isNullOrUndefined(campaign))
		{
			return campaign.poll;
		}
	}

	public async setPoll(guildId: string, campaignId: string, options: TStoragePoll, save: boolean = true): Promise<Error | undefined>
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(!isNullOrUndefined(campaign))
		{
			await this.setCampaignStatus(guildId, campaignId, CampaignStatus.VOTING, false);

			campaign.poll = options;

			if(save)
			{
				return this.save();
			}

			return;
		}

		return new Error(`No campaign with id \`${campaignId}\` found`);
	}

	public async cancelPoll(guildId: string, campaignId: string, save: boolean = true): Promise<Error | undefined>
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(isNullOrUndefined(campaign))
		{
			return new Error(`Could not find a campaign with the id \`${campaignId}\``);
		}

		const wasDeleted = Reflect.deleteProperty(campaign, 'poll');

		if(save && wasDeleted)
		{
			return this.save();
		}

		if(!wasDeleted)
		{
			return new Error('Could not remove data from database');
		}
	}

	public getNext(guildId: string, campaignId: string): IStorageNext | undefined
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(!isNullOrUndefined(campaign))
		{
			return campaign.next;
		}
	}

	public async setNext(guildId: string, campaignId: string, time: DateTime, members: User[] | undefined, unavailable: Set<string>, save: boolean = true): Promise<Error | undefined>
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(!isNullOrUndefined(campaign))
		{
			await this.setCampaignStatus(guildId, campaignId, CampaignStatus.SET, false);

			const currentMembers = campaign.next?.members;

			let newMembers = null;

			if(isNullOrUndefined(members))
			{
				if(isNullOrUndefined(currentMembers))
				{
					newMembers = new Set<string>();
				}
				else
				{
					newMembers = currentMembers;
				}
			}
			else
			{
				newMembers = new Set(members.map((member: User) =>
				{
					return member.id;
				}));
			}

			campaign.next = {
				time,
				cache: '',
				members: newMembers,
				unavailable,
			};

			if(save)
			{
				return this.save();
			}

			return;
		}

		return new Error(`No campaign with id \`${campaignId}\` found`);
	}

	public async setNextCache(guildId: string, campaignId: string, cache: string, save: boolean = true): Promise<Error | undefined>
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(!isNullOrUndefined(campaign) && !isNullOrUndefined(campaign.next))
		{
			campaign.next.cache = cache;
		}

		if(save)
		{
			return this.save();
		}
	}

	public async joinNext(guildId: string, campaignId: string, memberId: string, save: boolean = true): Promise<Error | undefined>
	{
		const next = this.getNext(guildId, campaignId);

		if(isNullOrUndefined(next))
		{
			return new Error(`The next session for the campaign with id \`${campaignId}\` is not set`);
		}

		if(!next.members.has(memberId))
		{
			next.members.add(memberId);

			if(next.unavailable.has(memberId))
			{
				next.unavailable.delete(memberId);
			}
		}
		else
		{
			return new Error('You have already marked yourself as available for this session');
		}

		if(save)
		{
			return this.save();
		}
	}

	public async leaveNext(guildId: string, campaignId: string, memberId: string, save: boolean = true): Promise<Error | undefined>
	{
		const next = this.getNext(guildId, campaignId);

		if(isNullOrUndefined(next))
		{
			return new Error(`The next session for the campaign with id \`${campaignId}\` is not set`);
		}

		if(!next.unavailable.has(memberId))
		{
			next.unavailable.add(memberId);

			if(next.members.has(memberId))
			{
				next.members.delete(memberId);
			}
		}
		else
		{
			return new Error('You have already marked yourself as unavailable for this session');
		}

		if(save)
		{
			return this.save();
		}
	}

	public async cancelNext(guildId: string, campaignId: string, save: boolean = true): Promise<Error | undefined>
	{
		const campaign = this._getCampaign(guildId, campaignId);

		if(isNullOrUndefined(campaign))
		{
			return new Error(`Could not find a campaign with the id \`${campaignId}\``);
		}

		const wasDeleted = Reflect.deleteProperty(campaign, 'next');

		if(save && wasDeleted)
		{
			return this.save();
		}

		if(!wasDeleted)
		{
			return new Error('Could not remove data from database');
		}
	}
}

export const storage = new StorageClass();
