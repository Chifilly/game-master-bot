import { isNullOrUndefined } from '../../helpers/utilities';
import type
{
	ISpecialJSONObject,
	ISpecialJSONObjectDateTime,
	ISpecialJSONObjectMap,
	ISpecialJSONObjectSet,
} from './interface';

export function isSpecialJSONObject(item: unknown): item is ISpecialJSONObject
{
	return typeof item === 'object' && !isNullOrUndefined(item) && '$type' in item && '$value' in item;
}

export function isSpecialJSONObjectMap(item: ISpecialJSONObject): item is ISpecialJSONObjectMap
{
	return item.$type === 'Map';
}

export function isSpecialJSONObjectSet(item: ISpecialJSONObject): item is ISpecialJSONObjectSet
{
	return item.$type === 'Set';
}

export function isSpecialJSONObjectDateTime(item: ISpecialJSONObject): item is ISpecialJSONObjectDateTime
{
	return item.$type === 'DateTime';
}
