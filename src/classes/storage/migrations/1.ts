/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */

import { isNullOrUndefined } from '../../../helpers/utilities';

export const version = 2;

export function migrate(data: any): any
{
	for(const [, guild] of data.guilds)
	{
		for(const [, campaign] of guild.campaigns)
		{
			campaign.leaders = new Set([campaign.leader]);
			Reflect.deleteProperty(campaign, 'leader');

			if(!isNullOrUndefined(campaign.next))
			{
				campaign.next.unavailable = new Set([]);
			}
		}
	}

	return data;
}
