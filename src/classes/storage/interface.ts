import type { DateTime } from 'luxon';

import type { CampaignStatus } from '.';

export type TStorageGuilds = Map<string, IStorageGuild>;
export type TStorageCampaigns = Map<string, IStorageCampaign>;
export type TStoragePoll = Map<string, DateTime>;

export type TStorageMemberList = Set<string>;

export type TStorageLeaders = TStorageMemberList;
export type TStorageManagers = TStorageMemberList;
export type TStorageMembers = TStorageMemberList;

export interface ISpecialJSONObject
{
	$type: string;
	$value: unknown;
}

export interface ISpecialJSONObjectMap extends ISpecialJSONObject
{
	$value: [unknown, unknown][];
}

export interface ISpecialJSONObjectSet extends ISpecialJSONObject
{
	$value: unknown[];
}

export interface ISpecialJSONObjectDateTime extends ISpecialJSONObject
{
	$value: string;
}

export interface IStorage
{
	version: number;
	guilds: TStorageGuilds;
}

export interface IStorageGuild
{
	roles: IStorageRoles;
	channels: IStorageChannels;
	reminders: IStorageReminders;
	campaigns: TStorageCampaigns;
}

export interface IStorageRoles
{
	leaders: Set<string>;
	managers: Set<string>;
}

export interface IStorageChannels
{
	poll?: string;
	reminder?: string;
}

export interface IStorageReminders
{
	enabled: boolean;
	times: IStorageReminderTime[];
}

export interface IStorageReminderTime
{
	text: string;
	number: number;
}

export interface IStorageCampaignBase
{
	name: string;
	leaders: TStorageMembers;
	status: CampaignStatus;
}

export interface IStorageCampaign extends IStorageCampaignBase
{
	message?: IStorageMessage;
	poll?: TStoragePoll;
	next?: IStorageNext;
}

export interface IStorageMessage
{
	id: string;
	channel: string;
	url: string;
}

export interface IStorageNext
{
	time: DateTime;
	cache: string;
	members: TStorageMembers;
	unavailable: TStorageMembers;
}
