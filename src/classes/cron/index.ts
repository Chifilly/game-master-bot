import { CronJob } from 'cron';

import { discord } from '../discord';
import { isNullOrUndefined } from '../../helpers/utilities';
import { label, logger } from '../logger';

interface ICronJobs
{
	[key: string]: CronJob;
}

class CronClass
{
	private readonly _jobs: ICronJobs = {};

	constructor()
	{
		// CRON TIME: seconds minutes hours day_of_month months day_of_week
		this._jobs = {
			messageUpdater: new CronJob('0 */1 * * * *', this._handleUpdate),
			presenceUpdates: new CronJob('0 0 * * * *', this._handlePresence),
		};
	}

	public async init(): Promise<void>
	{
		logger.info('Starting cron jobs', label.CRON);

		for(const jobKey of Object.keys(this._jobs))
		{
			const job = this._jobs[jobKey];

			job.start();
		}

		logger.info('All jobs started', label.CRON);
	}

	public startJob(jobName: string): void
	{
		const job = this._jobs[jobName];

		if(!isNullOrUndefined(job))
		{
			job.start();
		}
	}

	public stopJob(jobName: string): void
	{
		const job = this._jobs[jobName];

		if(!isNullOrUndefined(job))
		{
			job.stop();
		}
	}

	private _handleUpdate(this: void): void
	{
		discord.handleUpdateJob()
			.catch((e: Error) =>
			{
				logger.error(`Error executing update job: ${e.message}`, label.CRON);
			});
	}

	private _handlePresence(this: void): void
	{
		discord.setPresence();
	}
}

export const cron = new CronClass();
