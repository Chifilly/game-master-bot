import type { Logger } from 'winston';
import
{
	createLogger, format, transports, addColors,
} from 'winston';
import type { AbstractConfigSetLevels, AbstractConfigSetColors, AbstractConfigSet } from 'winston/lib/winston/config';
import type { Format, TransformableInfo, FormatWrap } from 'logform';
import colors from 'colors';

import { isNullOrUndefined } from '../../helpers/utilities';

export const color = {
	message:   colors.white,
	timestamp: colors.blue,
	misc:      colors.grey,
	method:    colors.yellow,
	success:   colors.green,
	fail:      colors.red,
	label:     colors.magenta,
	time:      colors.magenta,
	highlight: colors.green,
};

interface IAbstractConfigSetLevelNames
{
	error: string;
	warn: string;
	info: string;
	debug: string;
	route: string;
}

interface IAbstractConfigSetCustom extends AbstractConfigSet
{
	names: IAbstractConfigSetLevelNames;
}

const levelsSet: IAbstractConfigSetCustom = {
	levels: {
		error: 0,
		warn:  1,
		info:  2,
		debug: 3,
		route: 4,
	} as AbstractConfigSetLevels,
	colors: {
		error: 'red',
		warn:  'orange',
		info:  'green',
		debug: 'cyan',
		route: 'yellow',
	} as AbstractConfigSetColors,

	// Currently unused, but left here just in case I want to go back to using actual text
	names: {
		error: 'e',
		warn:  'w',
		info:  'i',
		debug: 'd',
		route: 'r',
	},
};
const topLevel: string = 'route';

const preLog: FormatWrap = format((info: TransformableInfo) =>
{
	const newInfo = { ...info };

	const debugLevelText = newInfo.level.toLowerCase() === 'debug' ? 'misc' : 'message';

	newInfo.timestamp = color.timestamp(newInfo.timestamp);
	newInfo.message = color[
		newInfo.level.toLowerCase() === 'error' || newInfo.level.toLowerCase() === 'warn' ? 'fail' : debugLevelText
	](newInfo.message);
	newInfo.label = isNullOrUndefined(newInfo.label) ? color.misc(newInfo.level) : color.label(newInfo.label);

	newInfo.level = '█';

	return newInfo;
});

const logFormat: Format = format.printf((info: TransformableInfo) =>
{
	return `${info.timestamp} ${info.level} [${info.label}] ${info.message}`;
});

addColors(levelsSet.colors);

/* eslint-disable @typescript-eslint/naming-convention */
export const label = {
	INIT:     { label: ' Startup' },
	DATABASE: { label: 'Database' },
	SERVER:   { label: '  Server' },
	DISCORD:  { label: ' Discord' },
	CONFIG:   { label: '  Config' },
	CRON:     { label: '    Cron' },
} as const;
/* eslint-enable @typescript-eslint/naming-convention */

export const logger: Logger = createLogger({
	level:  topLevel,
	levels: levelsSet.levels,
	format: format.combine(
		format.timestamp({
			format: 'DD-MM-YYYY HH:mm:ss',
		}),
		preLog(),
		format.colorize(),
		format.simple(),
		logFormat,
	),
	transports:        [new transports.Console()],
	exceptionHandlers: [new transports.Console()],
	exitOnError:       false,
});
