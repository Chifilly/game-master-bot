import type { ButtonInteraction } from 'discord.js';

import type { TInteractionElementsNotNull } from './handlers/guards';

export type TButtonHandler = (interaction: TInteractionElementsNotNull<ButtonInteraction>, campaignId: string) => void;

export class Button
{
	private readonly _path: string;

	private readonly _handler: TButtonHandler;

	constructor(
		path: string,
		handler: TButtonHandler,
	)
	{
		this._path = path;
		this._handler = handler;
	}

	public get path(): string
	{
		return this._path;
	}

	public get handle(): TButtonHandler
	{
		return this._handler;
	}
}
