import { Constants, MessageButton } from 'discord.js';

import type { Button } from './button';
import { buttons } from './handlers';

export function reminderJoinButton(campaignId: string): MessageButton
{
	return new MessageButton()
		.setCustomId(`reminder.join|${campaignId}`)
		.setLabel('Attending')
		.setStyle(Constants.MessageButtonStyles.PRIMARY);
}

export function reminderLeaveButton(campaignId: string): MessageButton
{
	return new MessageButton()
		.setCustomId(`reminder.leave|${campaignId}`)
		.setLabel('Can\'t Attend')
		.setStyle(Constants.MessageButtonStyles.DANGER);
}

// =====

export const buttonMap: Map<string, Button> = new Map();
for(const button of buttons)
{
	buttonMap.set(button.path, button);
}
