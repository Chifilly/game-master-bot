import type { CommandInteraction } from 'discord.js';

import type { TInteractionElementsNotNull } from './handlers/guards';

export enum AuthorityLevel
{
	OWNER = 0,
	MANAGER = 1,
	LEADER = 2,
	PUBLIC = 3,
}

export type TCommandHandler = (interaction: TInteractionElementsNotNull<CommandInteraction>) => void;

export class Command
{
	constructor(
		private readonly _path: string,
		private readonly _handler: TCommandHandler,
	)
	{ }

	public get path(): string
	{
		return this._path;
	}

	public get handle(): TCommandHandler
	{
		return this._handler;
	}
}
