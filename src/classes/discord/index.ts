import type
{
	Guild,
	TextChannel,
	ColorResolvable,
	Interaction,
	ApplicationCommand,
	GuildApplicationCommandPermissionData,
	ApplicationCommandPermissionData,
	ClientUser,
	ClientApplication,
	PresenceData,
	Presence,
	Message,
} from 'discord.js';
import
{
	Client,
	Intents,
	Constants,
	MessageEmbed,
} from 'discord.js';

import { logger, label } from '../logger';
import { isNullOrUndefined, isNullOrUndefinedOrEmpty } from '../../helpers/utilities';
import { config } from '../config';
import type { IStorageCampaign } from '../storage';
import { CampaignStatus, storage } from '../storage';
import { AuthorityLevel } from './command';
import
{
	slashCommands as commandArray,
	commandMap,
	permissionMap,
} from './commands';
import
{
	buildNextEmbed,
	buildPollEmbed,
	buildReminderActions,
	createCommandResponseMessage,
	createMentionedMembersListFromId,
	deleteCampaignMessage,
	getCampaignMessage,
	getRelativeNextDate,
	getTimeDifference,
	isInPast,
} from './helpers';
import { buttonMap } from './buttons';
import { interactionHasGuild, interactionHasMember } from './handlers/guards';
import { constants } from '../../helpers/constants';

// Map<[guildId], Map<[commandString], ApplicationCommand>>
type TCommandsMap = Map<string, Map<string, ApplicationCommand>>;

export class DiscordClass
{
	public static EMOJI = {
		date: '🗓️' as const,
		time: '🕔' as const,
		offset: '🌐' as const,
		timeFrom: '⏰' as const,
		reaction: new Set(['🇦', '🇧', '🇨', '🇩', '🇪', '🇫', '🇬', '🇭', '🇮', '🇯', '🇰', '🇱', '🇲', '🇳', '🇴', '🇵', '🇶', '🇷', '🇸', '🇹', '🇺', '🇻', '🇼', '🇽', '🇾', '🇿']),
		unavailable: '⛔' as const,
		random: '🔄' as const,
	};

	public static COLORS: { [key: string]: ColorResolvable } = {
		past: Constants.Colors.RED,
		future: Constants.Colors.GREEN,
		voting: Constants.Colors.ORANGE,
	};

	public static readonly REMINDER_COMPARISON_BUFFER: number = 5; // in seconds

	public static readonly REMINDER_EXPIRATION: number = 3600; // in seconds

	private readonly _client: Client;

	private _user!: ClientUser;

	private _application!: ClientApplication;

	private _isReady: boolean = false;

	private readonly _commands: TCommandsMap = new Map();

	private _logoutMessage: boolean = false;

	public get maxPollOptions(): number
	{
		return constants.numbers.MAX_EMBEDS_PER_MESSAGE - constants.numbers.ONE;
	}

	public get client(): Client
	{
		return this._client;
	}

	public get user(): ClientUser
	{
		return this._user;
	}

	public get application(): ClientApplication
	{
		return this._application;
	}

	constructor()
	{
		this._client = new Client({
			partials: [
				'MESSAGE',
				'CHANNEL',
				'REACTION',
			],
			intents: [
				Intents.FLAGS.GUILDS,
				Intents.FLAGS.GUILD_MESSAGES,
				Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
			],
		});

		this._client.on('error', (err) =>
		{
			logger.error(`${err.message}`, label.DISCORD);
		});

		this._client.once('ready', () =>
		{
			logger.info('Discord client ready', label.DISCORD);
		});

		this._client.on('guildCreate', async (guild: Guild) =>
		{
			await storage.initGuild(guild, false);
			await guild.channels.fetch();

			await this._mapGuildCommands(guild);

			await storage.save();
		});

		this._client.on('guildDelete', (guild: Guild) =>
		{
			const purge = storage.purgeGuild(guild.id);

			if(purge instanceof Error)
			{
				logger.error(`Error deleting guild: ${purge.message}`, label.DISCORD);
			}
		});

		this._client.on('interactionCreate', (interaction: Interaction) =>
		{
			if(interaction.isCommand())
			{
				if(!interactionHasGuild(interaction))
				{
					return interaction.reply(createCommandResponseMessage('Server not found', 'The command was not used in a server', true));
				}

				if(!interactionHasMember(interaction))
				{
					return interaction.reply(createCommandResponseMessage('Member not found', 'Could not find the member using the command', true));
				}

				const { commandName } = interaction;
				const commandSubcommandGroup = interaction.options.getSubcommandGroup(false);
				const commandSubCommand = interaction.options.getSubcommand(false);

				const commandString = `${commandName}${commandSubcommandGroup ? `.${commandSubcommandGroup}` : ''}${commandSubCommand ? `.${commandSubCommand}` : ''}`;

				const command = commandMap.get(commandString);

				if(!isNullOrUndefined(command))
				{
					return command.handle(interaction);
				}

				return interaction.reply(createCommandResponseMessage('Handler not found', 'Couldn\'t find a handler for the pressed button', true));
			}

			if(interaction.isButton())
			{
				if(!interactionHasGuild(interaction))
				{
					return interaction.reply(createCommandResponseMessage('Server not found', 'The command was not used in a server', true));
				}

				if(!interactionHasMember(interaction))
				{
					return interaction.reply(createCommandResponseMessage('Member not found', 'Could not find the member using the command', true));
				}

				const { customId } = interaction;

				const buttonParts = customId.split('|');

				const buttonString = buttonParts[0].trim();
				const campaignId = buttonParts[1].trim();

				const button = buttonMap.get(buttonString);

				if(!isNullOrUndefined(button))
				{
					return button.handle(interaction, campaignId);
				}

				return interaction.reply(createCommandResponseMessage('Handler not found', 'Couldn\'t find a handler for the pressed button', true));
			}

			if(interaction.isContextMenu())
			{
				return interaction.reply(createCommandResponseMessage('Doesn\'t exist', 'No context menus exist', true));
			}

			if(interaction.isSelectMenu())
			{
				const { customId } = interaction;

				const menuParts = customId.split('|');

				const menuString = menuParts[0].trim();

				if(menuString === 'response')
				{
					return undefined;
				}

				return interaction.reply(createCommandResponseMessage('Handler not found', 'Couldn\'t find a handler for the select menu', true));
			}
		});

		[
			'SIGHUP',
			'SIGINT',
			'SIGQUIT',
			'SIGILL',
			'SIGTRAP',
			'SIGABRT',
			'SIGBUS',
			'SIGFPE',
			'SIGUSR1',
			'SIGSEGV',
			'SIGUSR2',
			'SIGTERM',
		].forEach((signal) =>
		{
			process.once(signal, () =>
			{
				if(!this._logoutMessage)
				{
					this._logoutMessage = true;

					logger.info('Logging out', label.DISCORD);
				}

				this._client.destroy();

				process.exit();
			});
		});
	}

	public async init(): Promise<void>
	{
		logger.info('Initialising Discord', label.DISCORD);

		return this._client
			.login(config.token)
			.then(() =>
			{
				const { user, application } = this._client;

				if(isNullOrUndefined(user) || isNullOrUndefined(application))
				{
					throw new Error('No user and/or application on the Discord client');
				}

				this._user = user;
				this._application = application;

				logger.info(`Logged in as ${this._user?.tag}`, label.DISCORD);

				return this.setPresence();
			})
			.then(async () =>
			{
				await this._client.guilds.fetch();

				const guilds = this._client.guilds.cache;

				for(const [, guild] of guilds)
				{
					await storage.initGuild(guild, false);
					await guild.channels.fetch();

					const campaigns = storage.getCampaigns(guild.id);

					if(!isNullOrUndefined(campaigns))
					{
						for(const [campaignId, campaign] of campaigns)
						{
							if(campaign.status === CampaignStatus.SET)
							{
								await this.createOrUpdateNextEmbed(guild, campaignId, false);
							}
							else if(campaign.status === CampaignStatus.VOTING)
							{
								await this.createOrUpdatePollEmbed(guild, campaignId, false);
							}
						}
					}
				}

				return storage.save();
			})
			.then(async () =>
			{
				if(process.env.NODE_ENV !== 'development')
				{
					return this._application?.commands.set(commandArray)
						.then(async () =>
						{
							logger.info('Application commands registered', label.DISCORD);

							return this._mapCommands();
						})
						.then(() =>
						{
							logger.info('Commands ready', label.DISCORD);
						})
						.catch((e) =>
						{
							logger.error(`Error setting up application commands: ${e}`, label.DISCORD);
						});
				}

				return Promise.all(this._client.guilds.cache.map(async (guild) =>
				{
					return guild.commands.set(commandArray);
				}))
					.then(async () =>
					{
						logger.info('Guild commands registered', label.DISCORD);

						return this._mapCommands();
					})
					.then(() =>
					{
						logger.info('Commands ready', label.DISCORD);
					})
					.catch((e) =>
					{
						logger.error(`Error setting up guild commands: ${e}`, label.DISCORD);
					});
			})
			.then(() =>
			{
				return this.reload();
			})
			.then(async () =>
			{
				const dbGuilds = storage.getGuildIds();
				const guilds = this._client.guilds.cache;

				let wasDeleted = false;

				for(const guild of dbGuilds)
				{
					if(!guilds.has(guild))
					{
						const purge = storage.purgeGuild(guild, false);

						if(purge instanceof Error)
						{
							logger.error(`Error deleting guild: ${purge.message}`, label.DISCORD);
						}
						else
						{
							wasDeleted = true;
						}
					}
				}

				if(wasDeleted)
				{
					return storage.save();
				}
			})
			.then(() =>
			{
				logger.info('Discord successfully initialised', label.DISCORD);
			})
			.catch((e) =>
			{
				logger.error(e, label.DISCORD);

				throw new Error(e);
			});
	}

	public createInfoEmbed(title: string, message: string, error: boolean, footer: string | undefined = undefined): MessageEmbed
	{
		const embed = new MessageEmbed()
			.setTitle(error ? 'Error' : 'Info')
			.setColor(error ? Constants.Colors.RED : Constants.Colors.GREEN)
			.addField(title, message);

		if(!isNullOrUndefined(footer))
		{
			embed.setFooter(footer);
		}

		return embed;
	}

	public reload(): void
	{
		this._isReady = true;

		this.setPresence();
	}

	public setPresence(): Presence | undefined
	{
		let presenceData: PresenceData = {};

		if(this._isReady)
		{
			presenceData = {
				activities: [
					{
						name: '/',
						type: Constants.ActivityTypes.LISTENING,
					},
				],
				status: 'online',
				afk: false,
			};
		}
		else
		{
			presenceData = {
				activities: [],
				status: 'dnd',
				afk: true,
			};
		}

		return this._user?.setPresence(presenceData);
	}

	private async _mapCommands(): Promise<void>
	{
		const guilds = this._client.guilds.cache;

		for(const guildPair of guilds)
		{
			const guild = guildPair[1];

			await this._mapGuildCommands(guild);
		}

		logger.info('Command permissions set', label.DISCORD);
	}

	private async _mapGuildCommands(guild: Guild): Promise<void>
	{
		const guildCommandMap = new Map<string, ApplicationCommand>();

		const commands = process.env.NODE_ENV === 'development' ? guild.commands.cache : this._application.commands.cache;

		for(const commandPair of commands)
		{
			const command = commandPair[1];

			guildCommandMap.set(command.name, command);
		}

		this._commands.set(guild.id, guildCommandMap);

		await this.updateGuildPermissions(guild);
	}

	public async updateGuildPermissions(guild: Guild): Promise<void>
	{
		const commands = this._commands.get(guild.id);

		if(isNullOrUndefined(commands))
		{
			return;
		}

		const leaderRoles = storage.getLeaderRoles(guild.id);
		const managerRoles = storage.getManagerRoles(guild.id);

		const fullPermissions: GuildApplicationCommandPermissionData[] = [];

		const rolesAdded: Set<string> = new Set();

		for(const [, command] of commands)
		{
			rolesAdded.clear();

			const permissionString = permissionMap.get(command.name);

			if(!isNullOrUndefined(permissionString))
			{
				const owner = await guild.fetchOwner();
				const permissionToAdd: GuildApplicationCommandPermissionData = {
					id: command.id,
					permissions: [],
				};

				// Owners get all permissions
				permissionToAdd.permissions.push({
					id: owner.user.id,
					type: Constants.ApplicationCommandPermissionTypes.USER,
					permission: true,
				} as ApplicationCommandPermissionData);

				if(
					permissionString === AuthorityLevel.OWNER ||
					permissionString === AuthorityLevel.MANAGER ||
					permissionString === AuthorityLevel.LEADER
				)
				{
					if(
						permissionString === AuthorityLevel.MANAGER ||
						permissionString === AuthorityLevel.LEADER
					)
					{
						if(!isNullOrUndefined(managerRoles))
						{
							for(const roleId of managerRoles)
							{
								// Managers get the same permissions as leader plus their own

								if(!rolesAdded.has(roleId))
								{
									const permissionData: ApplicationCommandPermissionData = {
										id: roleId,
										type: Constants.ApplicationCommandPermissionTypes.ROLE,
										permission: true,
									};

									permissionToAdd.permissions.push(permissionData);

									rolesAdded.add(roleId);
								}
							}
						}

						if(permissionString === AuthorityLevel.LEADER)
						{
							if(!isNullOrUndefined(leaderRoles))
							{
								for(const roleId of leaderRoles)
								{
									// Leaders only get their own permissions

									if(!rolesAdded.has(roleId))
									{
										const permissionData: ApplicationCommandPermissionData = {
											id: roleId,
											type: Constants.ApplicationCommandPermissionTypes.ROLE,
											permission: true,
										};

										permissionToAdd.permissions.push(permissionData);

										rolesAdded.add(roleId);
									}
								}
							}
						}
					}
				}

				fullPermissions.push(permissionToAdd);
			}
		}

		await guild.commands.permissions.set({ fullPermissions });
	}

	public async createOrUpdateNextEmbed(guild: Guild, campaignId: string, save: boolean = true): Promise<Error | Message>
	{
		const guildId = guild.id;
		const next = storage.getNext(guildId, campaignId);

		if(isNullOrUndefined(next))
		{
			return new Error('Campaign session date hasn\'t been set');
		}

		const messageData = storage.getCampaignMessage(guildId, campaignId);
		const staticChannel = storage.getStaticChannel(guildId, 'reminder');

		let isNew: boolean = false;

		if(isNullOrUndefined(messageData))
		{
			isNew = true;
		}

		if(isNew && isNullOrUndefined(staticChannel))
		{
			return new Error('No reminder channel set');
		}

		const staticChannelChecked = isNullOrUndefined(staticChannel) ? undefined : staticChannel;
		const channelId = isNullOrUndefined(messageData) ? staticChannelChecked : messageData.channel;

		if(isNullOrUndefined(channelId))
		{
			return new Error('Could not find a valid channel id to post the message in');
		}

		const channel = guild.channels.cache.get(channelId) as TextChannel;

		if(isNullOrUndefined(channel))
		{
			return new Error('Error finding channel to post message in');
		}

		const embed = await buildNextEmbed(guildId, campaignId);

		if(!isNullOrUndefined(embed))
		{
			await channel.messages.fetch();

			const messageId = messageData?.id;

			let message = null;

			if(isNullOrUndefined(messageId))
			{
				isNew = true;
			}
			else
			{
				message = await getCampaignMessage(guildId, campaignId);
			}

			if(isNullOrUndefined(message))
			{
				isNew = true;
			}

			const relativeText = getRelativeNextDate(guildId, campaignId);

			const mentionsResult = await createMentionedMembersListFromId(guildId, campaignId);

			let mentions: string[] = [];

			if(!(mentionsResult instanceof Error))
			{
				mentions = mentionsResult;
			}

			const messageContent = {
				content: mentions?.join(' '),
				embeds: [embed],
				components: isInPast(next.time) ? [] : buildReminderActions(campaignId),
			};

			if(isNullOrUndefinedOrEmpty(messageContent.content))
			{
				messageContent.content = '.';
			}

			if(isNew)
			{
				try
				{
					message = await channel.send(messageContent);
				}
				catch(e)
				{
					return new Error('Error creating reminder message');
				}

				await storage.setCampaignMessage(guildId, campaignId, message, false);
				await storage.setNextCache(guildId, campaignId, relativeText, save);

				return message;
			}

			if(isNullOrUndefined(message))
			{
				return new Error('Error finding message');
			}

			try
			{
				await message.edit(messageContent);
			}
			catch(e)
			{
				return new Error('Error editing reminder message');
			}

			await storage.setNextCache(guildId, campaignId, relativeText, save);

			return message;
		}

		return new Error('Error building message embed');
	}

	public async createOrUpdatePollEmbed(guild: Guild, campaignId: string, save: boolean = true): Promise<Error | Message>
	{
		const guildId = guild.id;
		const poll = storage.getPoll(guildId, campaignId);

		if(isNullOrUndefined(poll))
		{
			return new Error('Campaign doesn\'t have a poll');
		}

		const messageData = storage.getCampaignMessage(guildId, campaignId);
		const staticChannel = storage.getStaticChannel(guildId, 'poll');

		let isNew: boolean = false;

		if(isNullOrUndefined(messageData))
		{
			isNew = true;
		}

		if(isNew && isNullOrUndefined(staticChannel))
		{
			return new Error('No poll channel set');
		}

		const staticChannelChecked = isNullOrUndefined(staticChannel) ? undefined : staticChannel;
		const channelId = isNullOrUndefined(messageData) ? staticChannelChecked : messageData.channel;

		if(isNullOrUndefined(channelId))
		{
			return new Error('Could not find a valid channel id to post the message in');
		}

		const channel = guild.channels.cache.get(channelId) as TextChannel;

		if(isNullOrUndefined(channel))
		{
			return new Error('Error finding channel to post message in');
		}

		for(const [emoji, time] of poll)
		{
			if(isInPast(time))
			{
				poll.delete(emoji);
			}
		}

		await storage.setPoll(guildId, campaignId, poll, save);

		await channel.messages.fetch();

		const messageId = messageData?.id;

		let message = null;

		if(isNullOrUndefined(messageId))
		{
			isNew = true;
		}
		else
		{
			message = await getCampaignMessage(guildId, campaignId);
		}

		if(isNullOrUndefined(message))
		{
			isNew = true;
		}

		const messageContent = {
			content: '@here',
			embeds: await buildPollEmbed(guildId, campaignId),
		};

		if(isNew)
		{
			try
			{
				message = await channel.send(messageContent);
			}
			catch(e)
			{
				logger.error(e);

				return new Error('Error creating poll message');
			}

			await storage.setCampaignMessage(guildId, campaignId, message, save);
		}

		if(isNullOrUndefined(message))
		{
			return new Error('Error finding message');
		}

		const reactions = message.reactions.cache;

		try
		{
			await message.edit(messageContent);
		}
		catch(e)
		{
			return new Error('Error editing poll message');
		}

		if(!reactions.has(DiscordClass.EMOJI.unavailable))
		{
			await message.react(DiscordClass.EMOJI.unavailable);
		}

		// Go through current reactions and remove any that aren't in the poll set
		for(const [emoji, reaction] of reactions)
		{
			if(emoji !== DiscordClass.EMOJI.unavailable && !poll.has(emoji))
			{
				await reaction.remove();
			}
		}

		// Go through the poll set and react to the message with any that are missing
		for(const [emoji] of poll)
		{
			if(!reactions.has(emoji))
			{
				await message.react(emoji);
			}
		}

		return message;
	}

	public async handleUpdateJob(): Promise<void>
	{
		const guilds = this._client.guilds.cache;

		let didUpdate: boolean = false;

		for(const [guildId, guild] of guilds)
		{
			const campaigns = storage.getCampaigns(guildId);

			if(isNullOrUndefined(campaigns))
			{
				continue;
			}

			for(const [campaignId, campaign] of campaigns)
			{
				let didThisUpdate: boolean = false;

				if(campaign.status === CampaignStatus.SET)
				{
					didThisUpdate = await this._handleReminder(guild, campaignId, campaign);
				}
				else if(campaign.status === CampaignStatus.VOTING)
				{
					didThisUpdate = await this._handlePoll(guild, campaignId, campaign);
				}

				if(didThisUpdate)
				{
					didUpdate = didThisUpdate;
				}
			}
		}

		if(didUpdate)
		{
			await storage.save();
		}
	}

	private async _handleReminder(guild: Guild, campaignId: string, campaign: IStorageCampaign): Promise<boolean>
	{
		const guildId = guild.id;

		let didUpdate: boolean = false;

		if(campaign.status !== CampaignStatus.SET)
		{
			return didUpdate;
		}

		const next = storage.getNext(guildId, campaignId);

		if(isNullOrUndefined(next))
		{
			return didUpdate;
		}

		const difference = getTimeDifference(next.time).as('seconds');
		const times = storage.getReminderTimes(guildId);

		// Delete after an hour has passed since the campaign ended
		if(difference < -(DiscordClass.REMINDER_COMPARISON_BUFFER + DiscordClass.REMINDER_EXPIRATION))
		{
			await deleteCampaignMessage(guildId, campaignId, false);

			didUpdate = true;

			return didUpdate;
		}

		if(isNullOrUndefined(times))
		{
			return didUpdate;
		}

		let shouldNotify = false;

		for(const time of times)
		{
			const comparison = time.number;

			if(difference <= comparison + DiscordClass.REMINDER_COMPARISON_BUFFER && difference >= comparison - DiscordClass.REMINDER_COMPARISON_BUFFER)
			{
				await deleteCampaignMessage(guildId, campaignId, false, false);

				didUpdate = true;
				shouldNotify = true;

				break;
			}
		}

		const message = await getCampaignMessage(guildId, campaignId);
		const relativeText = getRelativeNextDate(guildId, campaignId);

		if(isNullOrUndefined(message) || shouldNotify || relativeText !== next.cache)
		{
			await this.createOrUpdateNextEmbed(guild, campaignId, false);

			didUpdate = true;
		}

		return didUpdate;
	}

	private async _handlePoll(guild: Guild, campaignId: string, campaign: IStorageCampaign): Promise<boolean>
	{
		const guildId = guild.id;

		let didUpdate: boolean = false;

		if(campaign.status !== CampaignStatus.VOTING)
		{
			return didUpdate;
		}

		const next = storage.getNext(guildId, campaignId);

		if(isNullOrUndefined(next))
		{
			return didUpdate;
		}

		const message = await getCampaignMessage(guildId, campaignId);

		if(isNullOrUndefined(message))
		{
			await this.createOrUpdatePollEmbed(guild, campaignId, false);

			didUpdate = true;
		}

		return didUpdate;
	}
}

export const discord = new DiscordClass();
