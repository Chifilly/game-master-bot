import type { Command } from '../command';
import type { Button } from '../button';
import { command as setupManagersAdd } from './commands/setup/managers/add';
import { command as setupManagersRemove } from './commands/setup/managers/remove';
import { command as setupManagersList } from './commands/setup/managers/list';
import { command as settingsLeadersAdd } from './commands/settings/leaders/add';
import { command as settingsLeadersRemove } from './commands/settings/leaders/remove';
import { command as settingsLeadersList } from './commands/settings/leaders/list';
import { command as settingsPollsChannel } from './commands/settings/polls/channel';
import { command as settingsRemindersChannel } from './commands/settings/reminders/channel';
import { command as settingsRemindersList } from './commands/settings/reminders/list';
import { command as settingsRemindersSet } from './commands/settings/reminders/set';
import { command as settingsRemindersEnabled } from './commands/settings/reminders/enabled';
import { command as campaignNew } from './commands/campaign/new';
import { command as campaignRename } from './commands/campaign/rename';
import { command as campaignDelete } from './commands/campaign/delete';
import { command as campaignSetNext } from './commands/next/set';
import { command as campaignNextCancel } from './commands/next/cancel';
import { command as pollNew } from './commands/poll/new';
import { command as pollCancel } from './commands/poll/cancel';
import { command as pollClose } from './commands/poll/close';
import { command as pollEditAdd } from './commands/poll/edit/add';
import { command as pollEditChange } from './commands/poll/edit/change';
import { command as pollEditRemove } from './commands/poll/edit/remove';
import { command as campaigns } from './commands/campaigns';
import { button as reminderJoin } from './buttons/reminder/join';
import { button as reminderLeave } from './buttons/reminder/leave';

export const commands: Command[] = [
	setupManagersAdd,
	setupManagersRemove,
	setupManagersList,

	settingsLeadersAdd,
	settingsLeadersRemove,
	settingsLeadersList,
	settingsPollsChannel,
	settingsRemindersChannel,
	settingsRemindersList,
	settingsRemindersSet,
	settingsRemindersEnabled,

	campaignNew,
	campaignRename,
	campaignDelete,
	campaignSetNext,
	campaignNextCancel,

	pollNew,
	pollCancel,
	pollClose,
	pollEditAdd,
	pollEditChange,
	pollEditRemove,

	campaigns,
];

export const buttons: Button[] = [
	reminderJoin,
	reminderLeave,
];
