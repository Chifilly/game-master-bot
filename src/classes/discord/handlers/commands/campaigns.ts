import type { TCommandHandler } from '../../command';
import { Command } from '../../command';
import { buildCampaignsEmbed } from '../../helpers';

const path: string = 'campaigns';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const embed = await buildCampaignsEmbed(interaction.guildId);

	return interaction.reply({
		embeds:    [embed],
		ephemeral: true,
	});
};

export const command = new Command(path, handler);
