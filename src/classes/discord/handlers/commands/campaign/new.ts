import type { TCommandHandler } from '../../../command';
import { Command } from '../../../command';
import { storage } from '../../../../storage';
import { isNullOrUndefined } from '../../../../../helpers/utilities';
import { createCommandResponseMessage } from '../../../helpers';

const path: string = 'campaign.new';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const nameArg = interaction.options.getString('name');

	if(isNullOrUndefined(nameArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You have not provided a campaign name', true));
	}

	const name = nameArg.trim();

	const result = await storage.createCampaign(interaction.guild.id, interaction.user.id, name);

	if(result instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t update database', result.message, true));
	}

	return interaction.reply(createCommandResponseMessage('Campaign created', `Successfully created a campaign with the name \`${name}\`, and its id is the following:\n\`${result}\``));
};

export const command = new Command(path, handler);
