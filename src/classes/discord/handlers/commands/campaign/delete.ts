import type { TCommandHandler } from '../../../command';
import { Command } from '../../../command';
import { storage } from '../../../../storage';
import { isNullOrUndefined } from '../../../../../helpers/utilities';
import { createCommandResponseMessage, deleteCampaignMessage } from '../../../helpers';

const path: string = 'campaign.delete';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const campaignIdArg = interaction.options.getString('id');

	if(isNullOrUndefined(campaignIdArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You have not provided a campaign id', true));
	}

	if(!storage.doesCampaignExist(interaction.guildId, campaignIdArg))
	{
		return interaction.reply(createCommandResponseMessage('Campaign not found', `Could not find a campaign with the id \`${campaignIdArg}\``, true));
	}

	if(!storage.isUserCampaignLeader(interaction.guildId, campaignIdArg, interaction.user.id))
	{
		return interaction.reply(createCommandResponseMessage('No permission', 'You are not the leader of this campaign', true));
	}

	await deleteCampaignMessage(interaction.guildId, campaignIdArg, false);

	const resultDatabase = await storage.deleteCampaign(interaction.guildId, campaignIdArg);

	if(resultDatabase instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t update database', resultDatabase.message, true));
	}

	return interaction.reply(createCommandResponseMessage('Deleted campaign', `Successfully deleted campaign with id \`${campaignIdArg}\``));
};

export const command = new Command(path, handler);
