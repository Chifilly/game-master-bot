import type { TCommandHandler } from '../../../command';
import { Command } from '../../../command';
import { storage } from '../../../../storage';
import { isNullOrUndefined } from '../../../../../helpers/utilities';
import { createCommandResponseMessage } from '../../../helpers';
import { discord } from '../../..';

const path: string = 'campaign.rename';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const campaignIdArg = interaction.options.getString('id');
	const nameArg = interaction.options.getString('name');

	if(isNullOrUndefined(campaignIdArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You have not provided a campaign id', true));
	}

	if(isNullOrUndefined(nameArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You have not provided a campaign name', true));
	}

	if(!storage.doesCampaignExist(interaction.guildId, campaignIdArg))
	{
		return interaction.reply(createCommandResponseMessage('Campaign not found', `Could not find a campaign with the id \`${campaignIdArg}\``, true));
	}

	const isOwner = interaction.guild.ownerId === interaction.user.id;
	const isManager = storage.isMemberManager(interaction.guildId, interaction.member);
	const isLeader = storage.isUserCampaignLeader(interaction.guildId, campaignIdArg, interaction.user.id);

	if(!isOwner && !isManager && !isLeader)
	{
		return interaction.reply(createCommandResponseMessage('No permission', 'You are not the leader of this campaign', true));
	}

	const name = nameArg.trim();

	const result = await storage.renameCampaign(interaction.guildId, campaignIdArg, name, false);

	if(result instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t update database', result.message, true));
	}

	await discord.createOrUpdateNextEmbed(interaction.guild, campaignIdArg, true);

	return interaction.reply(createCommandResponseMessage('Renamed campaign', `Successfully renamed campaign with id \`${campaignIdArg}\` to the following:\n\`${name}\``));
};

export const command = new Command(path, handler);
