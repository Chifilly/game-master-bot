import type { TCommandHandler } from '../../../../command';
import { Command } from '../../../../command';
import type { IStorageReminderTime } from '../../../../../storage';
import { storage } from '../../../../../storage';
import { isNullOrUndefined } from '../../../../../../helpers/utilities';
import { createCommandResponseMessage, parseReminderString } from '../../../../helpers';
import { constants } from '../../../../../../helpers/constants';

const path: string = 'settings.reminders.set';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const timesArg = interaction.options.getString('times');

	if(isNullOrUndefined(timesArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You have not provided any times', true));
	}

	const timesSplit = timesArg.split(' ');

	const errors: string[] = [];
	const times: IStorageReminderTime[] = [];

	for(const time of timesSplit)
	{
		const parsed = parseReminderString(time);

		if(!parsed.isValid)
		{
			errors.push(time);

			continue;
		}

		times.push({
			text: time,
			number: parsed.as('seconds'),
		});
	}

	let timesStrings: string[] = [];

	if(times.length > constants.numbers.ZERO)
	{
		const result = await storage.setReminderTimes(interaction.guildId, times);

		if(result instanceof Error)
		{
			return interaction.reply(createCommandResponseMessage('Couldn\'t set times', result.message, true));
		}

		timesStrings = result
			.map((time) =>
			{
				return `\`${time.text}\``;
			});
	}

	const errorsStrings: string[] = errors
		.map((error) =>
		{
			return `\`${error}\``;
		});

	let replyString = '';

	if(timesStrings.length > constants.numbers.ZERO)
	{
		replyString = `Successfully set the reminder times to the following:\n${timesStrings.join(' ')}`;
	}
	else
	{
		replyString = 'No valid reminder times were supplied';
	}

	if(errors.length > constants.numbers.ZERO)
	{
		replyString += `\n\nThe following reminder times were invalid:\n${errorsStrings.join(' ')}`;
	}

	return interaction.reply(createCommandResponseMessage(
		timesStrings.length <= constants.numbers.ZERO ? 'Couldn\'t set reminder times' : 'Reminder times set',
		replyString,
		timesStrings.length <= constants.numbers.ZERO,
	));
};

export const command = new Command(path, handler);
