import type { TCommandHandler } from '../../../../command';
import { Command } from '../../../../command';
import { storage } from '../../../../../storage';
import { isNullOrUndefined } from '../../../../../../helpers/utilities';
import { createCommandResponseMessage } from '../../../../helpers';
import { constants } from '../../../../../../helpers/constants';

const path: string = 'settings.reminders.list';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const times = storage.getReminderTimes(interaction.guildId);

	if(isNullOrUndefined(times))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'Could not get reminder times', true));
	}

	if(times.length <= constants.numbers.ZERO)
	{
		return interaction.reply(createCommandResponseMessage('Reminder times', 'There are currently no reminder times set'));
	}

	const timesStrings: string[] = times
		.map((time) =>
		{
			return `\`${time.text}\``;
		});

	return interaction.reply(createCommandResponseMessage('Reminder times', `The currently set reminder times are as follows:\n${timesStrings.join(' ')}`));
};

export const command = new Command(path, handler);
