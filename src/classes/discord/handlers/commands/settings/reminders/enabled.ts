import type { TCommandHandler } from '../../../../command';
import { Command } from '../../../../command';
import { storage } from '../../../../../storage';
import { isNullOrUndefined } from '../../../../../../helpers/utilities';
import { createCommandResponseMessage } from '../../../../helpers';

const path: string = 'settings.reminders.enabled';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const isEnabledArg = interaction.options.getBoolean('enabled');

	if(isNullOrUndefined(isEnabledArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You have not provided an enabled value', true));
	}

	const result = await storage.setRemindersEnabled(interaction.guildId, isEnabledArg);

	if(result instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t update database', result.message, true));
	}

	return interaction.reply(createCommandResponseMessage('Changed reminder state', `Successfully ${isEnabledArg ? 'enabled' : 'disabled'} reminders`));
};

export const command = new Command(path, handler);
