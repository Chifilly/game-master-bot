import type { Role } from 'discord.js';

import type { TCommandHandler } from '../../../../command';
import { Command } from '../../../../command';
import { storage } from '../../../../../storage';
import { isNullOrUndefined } from '../../../../../../helpers/utilities';
import { createCommandResponseMessage } from '../../../../helpers';

const path: string = 'settings.leaders.remove';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const roleArg = interaction.options.getRole('role');

	if(isNullOrUndefined(roleArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You did not provide a role to remove', true));
	}

	const result = await storage.removeLeaderRole(interaction.guildId, roleArg as Role);

	if(result instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t update database', result.message, true));
	}

	return interaction.reply(createCommandResponseMessage('Leader removed', `Successfully removed ${roleArg} from leadership roles`));
};

export const command = new Command(path, handler);
