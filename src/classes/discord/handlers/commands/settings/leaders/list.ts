import type { TCommandHandler } from '../../../../command';
import { Command } from '../../../../command';
import { storage } from '../../../../../storage';
import { isNullOrUndefined } from '../../../../../../helpers/utilities';
import { createCommandResponseMessage } from '../../../../helpers';
import { constants } from '../../../../../../helpers/constants';

const path: string = 'settings.leaders.list';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const leaderRoles = storage.getLeaderRoles(interaction.guildId);

	if(isNullOrUndefined(leaderRoles) || leaderRoles.size <= constants.numbers.ZERO)
	{
		return interaction.reply(createCommandResponseMessage('Leaders', 'There are currently no leadership roles assigned'));
	}

	const roles = interaction.guild.roles.cache.filter((role) =>
	{
		return leaderRoles.has(role.id);
	});

	return interaction.reply(createCommandResponseMessage('Leaders', `The current leadership roles are as follows:\n${[...roles.values()].join(' ')}`));
};

export const command = new Command(path, handler);
