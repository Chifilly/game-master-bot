import { Permissions } from 'discord.js';

import type { TCommandHandler } from '../../../../command';
import { Command } from '../../../../command';
import { storage } from '../../../../../storage';
import { isNullOrUndefined } from '../../../../../../helpers/utilities';
import { createCommandResponseMessage, requiredPollChannelPermissions } from '../../../../helpers';
import { discord } from '../../../..';
import { constants } from '../../../../../../helpers/constants';

const path: string = 'settings.polls.channel';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const channelArg = interaction.options.getChannel('channel');

	if(isNullOrUndefined(channelArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'Could not find the guild the commands was used in', true));
	}

	const channel = interaction.guild.channels.cache.get(channelArg.id);

	if(isNullOrUndefined(channel))
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t find channel', 'Could not find the specified channel', true));
	}

	if(channel.type !== 'GUILD_TEXT')
	{
		return interaction.reply(createCommandResponseMessage('Invalid channel', `${channelArg} is not a valid channel for polls. It must be a text channel`, true));
	}

	const permissions = channel.permissionsFor(discord.user);

	if(isNullOrUndefined(permissions))
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t get permissions', `Could not get permissions for ${channel}`, true));
	}

	const requiredPermissions: bigint[] = [...requiredPollChannelPermissions.keys()];

	const missingPermissions = permissions.missing(requiredPermissions);

	if(missingPermissions.length > constants.numbers.ZERO)
	{
		let text = `${channelArg} is not a valid channel for polls, because I don't have all the required permissions. The following permissions are missing:\n\n`;

		let index = 0;
		for(const permission of missingPermissions)
		{
			const resolved: bigint = Permissions.resolve(permission);
			const permissionData = requiredPollChannelPermissions.get(resolved);

			if(isNullOrUndefined(permissionData))
			{
				text += '**Unknown permission**\n';
				text += 'Error finding permission information. This is most likely a bug';

				continue;
			}

			text += `**${permissionData[0]}**\n${permissionData[1]}`;

			if(index !== missingPermissions.length - constants.numbers.ONE)
			{
				text += '\n\n';
			}

			index++;
		}

		return interaction.reply(createCommandResponseMessage('Missing permissions', text, true));
	}

	const result = await storage.setStaticChannel(interaction.guild.id, 'poll', channelArg.id);

	if(result instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t update database', result.message, true));
	}

	return interaction.reply(createCommandResponseMessage('Set poll channel', `Successfully set the poll channel to ${channelArg}`));
};

export const command = new Command(path, handler);
