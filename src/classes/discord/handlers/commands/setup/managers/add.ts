import type { Role } from 'discord.js';

import { discord } from '../../../..';
import type { TCommandHandler } from '../../../../command';
import { Command } from '../../../../command';
import { storage } from '../../../../../storage';
import { isNullOrUndefined } from '../../../../../../helpers/utilities';
import { createCommandResponseMessage } from '../../../../helpers';

const path: string = 'setup.managers.add';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const roleArg = interaction.options.getRole('role');

	if(isNullOrUndefined(roleArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You did not provide a role to add', true));
	}

	const result = await storage.addManagerRole(interaction.guildId, roleArg as Role);

	if(result instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t update database', result.message, true));
	}

	await discord.updateGuildPermissions(interaction.guild);

	return interaction.reply(createCommandResponseMessage('Manager added', `Successfully added ${roleArg} to manager roles`));
};

export const command = new Command(path, handler);
