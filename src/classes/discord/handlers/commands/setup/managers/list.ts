import type { TCommandHandler } from '../../../../command';
import { Command } from '../../../../command';
import { storage } from '../../../../../storage';
import { isNullOrUndefined } from '../../../../../../helpers/utilities';
import { createCommandResponseMessage } from '../../../../helpers';
import { constants } from '../../../../../../helpers/constants';

const path: string = 'setup.managers.list';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const managerRoles = storage.getManagerRoles(interaction.guildId);

	if(isNullOrUndefined(managerRoles) || managerRoles.size <= constants.numbers.ZERO)
	{
		return interaction.reply(createCommandResponseMessage('Managers', 'There are currently no manager roles assigned'));
	}

	const roles = interaction.guild.roles.cache.filter((role) =>
	{
		return managerRoles.has(role.id);
	});

	return interaction.reply(createCommandResponseMessage('Managers', `The current manager roles are as follows:\n${[...roles.values()].join(' ')}`));
};

export const command = new Command(path, handler);
