import type { DateTime } from 'luxon';

import { discord, DiscordClass } from '../../../..';
import { constants } from '../../../../../../helpers/constants';
import { isNullOrUndefined } from '../../../../../../helpers/utilities';
import { storage } from '../../../../../storage';
import type { TCommandHandler } from '../../../../command';
import { Command } from '../../../../command';
import { createCommandResponseMessage, isInPast, parseDate } from '../../../../helpers';

const path: string = 'poll.edit.add';

const handler: TCommandHandler = async (interaction): Promise<unknown> =>
{
	await interaction.deferReply({ ephemeral: true });

	const campaignIdArg = interaction.options.getString('id');
	const timestampsArg = interaction.options.getString('timestamps');

	if(isNullOrUndefined(campaignIdArg))
	{
		return interaction.editReply(createCommandResponseMessage('Missing parameter', 'You have not provided a campaign id', true));
	}

	if(isNullOrUndefined(timestampsArg))
	{
		return interaction.editReply(createCommandResponseMessage('Missing parameter', 'You have not provided any timestamps', true));
	}

	if(!storage.doesCampaignExist(interaction.guildId, campaignIdArg))
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t find campaign', `Could not find a campaign with the id \`${campaignIdArg}\``, true));
	}

	if(!storage.isUserCampaignLeader(interaction.guildId, campaignIdArg, interaction.user.id))
	{
		return interaction.reply(createCommandResponseMessage('No permission', 'You are not the leader of this campaign', true));
	}

	const timestampSections: string[] = timestampsArg
		.split('|')
		.map((timestamp) =>
		{
			return timestamp.trim();
		})
		.filter((timestamp: string) =>
		{
			return !isNullOrUndefined(timestamp);
		});

	const pollDates: DateTime[] = [];
	const errors: string[] = [];

	for(const timestamp of timestampSections)
	{
		const date: DateTime = parseDate(timestamp);

		if(!date.isValid)
		{
			errors.push(`\`${timestamp}\` [Invalid format]`);

			continue;
		}

		if(isInPast(date))
		{
			errors.push(`\`${timestamp}\` [In the past]`);

			continue;
		}

		pollDates.push(date);
	}

	if(errors.length > constants.numbers.ZERO)
	{
		let message = `There ${errors.length === constants.numbers.ONE ? 'is' : 'are'} ${errors.length} invalid timestamp${errors.length === constants.numbers.ONE ? '' : 's'}`;
		message += `, which ${errors.length === constants.numbers.ONE ? 'is' : 'are'} as follows:\n`;

		message += errors
			.join('\n');

		return interaction.editReply(createCommandResponseMessage('Invalid timestamps', message, true));
	}

	if(pollDates.length <= constants.numbers.ZERO)
	{
		return interaction.editReply(createCommandResponseMessage('Missing timestamps', 'You have not provided any timestamps', true));
	}

	const poll = storage.getPoll(interaction.guildId, campaignIdArg);

	if(poll instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t find poll', poll.message, true));
	}

	if(isNullOrUndefined(poll))
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t find poll', `Could not find the poll for the campaign with id \`${campaignIdArg}\` in the database`, true));
	}

	if(pollDates.length + poll.size > discord.maxPollOptions)
	{
		let messageText: string = 'You are trying to add too many options to the poll\n\n';
		messageText += `There is a limit of \`${discord.maxPollOptions}\` options, the poll currently has \`${poll.size}\`, and you tried to add \`${pollDates.length}\` which would exceed the limit\n\n`;
		messageText += 'Try adding fewer options or deleting existing options and try again';

		return interaction.editReply(createCommandResponseMessage('Too many poll options', messageText, true));
	}

	const sortedPoll: DateTime[] = pollDates.sort((a, b) =>
	{
		return a.toMillis() - b.toMillis();
	});

	const emojiSet = [...DiscordClass.EMOJI.reaction.values()]
		.filter((item) =>
		{
			return !poll.has(item);
		});

	for(let i = 0; i < sortedPoll.length; i++)
	{
		const date = sortedPoll[i];

		poll.set(emojiSet[i], date);
	}

	const databaseResult = await storage.setPoll(interaction.guildId, campaignIdArg, poll, true);

	if(databaseResult instanceof Error)
	{
		return interaction.editReply(createCommandResponseMessage('Couldn\'t update database', databaseResult.message, true));
	}

	const messageResult = await discord.createOrUpdatePollEmbed(interaction.guild, campaignIdArg, true);

	if(messageResult instanceof Error)
	{
		return interaction.editReply(createCommandResponseMessage('Couldn\'t edit message', messageResult.message, true));
	}

	return interaction.editReply(createCommandResponseMessage('Poll edited', `Successfully created poll for the campaign with id \`${campaignIdArg}\``));
};

export const command = new Command(path, handler);
