import type { DateTime } from 'luxon';

import { discord } from '../../../..';
import { isNullOrUndefined } from '../../../../../../helpers/utilities';
import { storage } from '../../../../../storage';
import type { TCommandHandler } from '../../../../command';
import { Command } from '../../../../command';
import { createCommandResponseMessage } from '../../../../helpers';

const path: string = 'poll.edit.remove';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const campaignIdArg = interaction.options.getString('id');
	const emojiArg = interaction.options.getString('emoji');

	if(isNullOrUndefined(campaignIdArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You have not provided a campaign id', true));
	}

	if(isNullOrUndefined(emojiArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You have not provided an emoji', true));
	}

	if(!storage.doesCampaignExist(interaction.guildId, campaignIdArg))
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t find campaign', `Could not find a campaign with the id \`${campaignIdArg}\``, true));
	}

	if(!storage.isUserCampaignLeader(interaction.guildId, campaignIdArg, interaction.user.id))
	{
		return interaction.reply(createCommandResponseMessage('No permission', 'You are not the leader of this campaign', true));
	}

	const poll = storage.getPoll(interaction.guildId, campaignIdArg);

	if(poll instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t find poll', poll.message, true));
	}

	if(isNullOrUndefined(poll))
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t find poll', `Could not find the poll for the campaign with id \`${campaignIdArg}\` in the database`, true));
	}

	if(!poll.has(emojiArg))
	{
		return interaction.reply(createCommandResponseMessage('No option', `The poll for the campaign with id \`${campaignIdArg}\` doesn't have an option for ${emojiArg}`, true));
	}

	const lastOption = poll.get(emojiArg) as DateTime;
	const wasDeleted = poll.delete(emojiArg);

	if(!wasDeleted)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t delete option', 'Error deleting poll option', true));
	}

	const saveResult = await storage.setPoll(interaction.guildId, campaignIdArg, poll, false);

	if(saveResult instanceof Error)
	{
		poll.set(emojiArg, lastOption);

		await storage.setPoll(interaction.guildId, campaignIdArg, poll);

		return interaction.reply(createCommandResponseMessage('Couldn\'t update database', saveResult.message, true));
	}

	const messageResult = await discord.createOrUpdatePollEmbed(interaction.guild, campaignIdArg, true);

	if(messageResult instanceof Error)
	{
		poll.set(emojiArg, lastOption);

		await storage.setPoll(interaction.guildId, campaignIdArg, poll, true);

		return interaction.reply(createCommandResponseMessage('Couldn\'t edit message', messageResult.message, true));
	}

	return interaction.reply(createCommandResponseMessage('Poll edited', `Successfully deleted option ${emojiArg} from poll with id \`${campaignIdArg}\``));
};

export const command = new Command(path, handler);
