import type
{
	CommandInteraction,
	Message,
	MessageReaction,
	MessageSelectOptionData,
	SelectMenuInteraction,
	User,
} from 'discord.js';
import
{
	Constants,
	MessageActionRow,
	MessageSelectMenu,
} from 'discord.js';
import { DateTime } from 'luxon';

import { discord, DiscordClass } from '../../..';
import { Command } from '../../../command';
import type { TCommandHandler } from '../../../command';
import { CampaignStatus, storage } from '../../../../storage';
import { isNullOrUndefined, random } from '../../../../../helpers/utilities';
import { constants as CustomConstants } from '../../../../../helpers/constants';
import
{
	createCommandResponseMessage,
	deleteCampaignMessage,
	getCampaignMessage,
	isInPast,
	outputTimeFormat,
} from '../../../helpers';
import type { TInteractionElementsNotNull } from '../../guards';

const path: string = 'poll.close';

const handler: TCommandHandler = async (interaction): Promise<unknown> =>
{
	await interaction.deferReply({ ephemeral: true });

	const campaignIdArg = interaction.options.getString('id');

	if(isNullOrUndefined(campaignIdArg))
	{
		return interaction.editReply(createCommandResponseMessage('Missing parameter', 'You have not provided a campaign id', true));
	}

	if(!storage.doesCampaignExist(interaction.guildId, campaignIdArg))
	{
		return interaction.editReply(createCommandResponseMessage('Couldn\'t find campaign', `Could not find a campaign with the id \`${campaignIdArg}\``, true));
	}

	if(!storage.isUserCampaignLeader(interaction.guildId, campaignIdArg, interaction.user.id))
	{
		return interaction.reply(createCommandResponseMessage('No permission', 'You are not the leader of this campaign', true));
	}

	const campaignInfo = storage.getCampaignInfo(interaction.guildId, campaignIdArg);

	if(isNullOrUndefined(campaignInfo))
	{
		return interaction.editReply(createCommandResponseMessage('Couldn\'t find campaign', 'Could not get campaign information from the database', true));
	}

	if(campaignInfo.status !== CampaignStatus.VOTING)
	{
		return interaction.editReply(createCommandResponseMessage('No poll', `The campaign with id \`${campaignIdArg}\` doesn't have a poll`, true));
	}

	const message = await getCampaignMessage(interaction.guildId, campaignIdArg);

	if(isNullOrUndefined(message))
	{
		return interaction.editReply(createCommandResponseMessage('Couldn\'t find message', 'Couldn\'t find the poll message', true));
	}

	if(message.partial)
	{
		await message.fetch();
	}

	let topReactionVotes: number = 0;
	const topReactions: Map<string, MessageReaction> = new Map();

	const emojiSet = DiscordClass.EMOJI.reaction;
	const poll = storage.getPoll(interaction.guildId, campaignIdArg);

	const unavailableUsers: Set<string> = new Set();

	if(isNullOrUndefined(poll))
	{
		return interaction.editReply(createCommandResponseMessage('Couldn\'t find poll', 'Couldn\'t find poll data in the database', true));
	}

	for(const [, reaction] of message.reactions.cache)
	{
		const emoji = reaction.emoji.toString();

		if(!emojiSet.has(emoji))
		{
			if(emoji === DiscordClass.EMOJI.unavailable)
			{
				for(const user of reaction.users.cache.values())
				{
					if(user.id !== discord.client.user?.id)
					{
						unavailableUsers.add(user.id);
					}
				}
			}

			continue;
		}

		if(reaction.count > topReactionVotes)
		{
			topReactionVotes = reaction.count;
			topReactions.clear();
		}

		for(const unavailableUserId of unavailableUsers)
		{
			if(reaction.users.cache.has(unavailableUserId))
			{
				unavailableUsers.delete(unavailableUserId);
			}
		}

		const time = poll.get(emoji);

		if(isNullOrUndefined(time) || isInPast(time))
		{
			continue;
		}

		if(reaction.count === topReactionVotes)
		{
			topReactions.set(emoji, reaction);
		}
	}

	let topReaction: MessageReaction | null = null;

	if(topReactions.size > CustomConstants.numbers.ONE)
	{
		const randomOption: MessageSelectOptionData = {
			emoji: DiscordClass.EMOJI.random,
			label: 'Set one at random',
			value: DiscordClass.EMOJI.random,
		};

		const row = new MessageActionRow()
			.addComponents(
				new MessageSelectMenu()
					.setCustomId('response|multipleNext')
					.setPlaceholder('Choose an option')
					.addOptions([
						...[
							...topReactions.values(),
						]
							.map((reaction): MessageSelectOptionData =>
							{
								return {
									emoji: reaction.emoji.toString(),
									label: 'Set to this option',
									value: reaction.emoji.toString(),
								};
							}),
						randomOption,
					]),
			);

		const responseMessage: Message = await interaction.editReply({
			embeds: [discord.createInfoEmbed(
				'Votes are tied',
				'There are multiple dates and times with equal votes\n\nPlease choose the one you want to apply to the next session',
				false,
				`This message expires at ${DateTime.utc().plus({ minutes: 1 }).toFormat(outputTimeFormat)}`,
			)],
			components: [row],
		}) as Message;

		const filter = async (selectInteraction: SelectMenuInteraction) =>
		{
			await selectInteraction.deferUpdate();

			return selectInteraction.user.id === interaction.user.id;
		};

		let collection: SelectMenuInteraction | null = null;

		try
		{
			collection = await responseMessage.awaitMessageComponent({
				filter,
				componentType: Constants.MessageComponentTypes.SELECT_MENU,
				time: 60000,
				dispose: true,
			});
		}
		catch(e: unknown)
		{
			return interaction.editReply(createCommandResponseMessage('Timed out', 'Didn\'t receive a response within 1 minute', true));
		}

		await interaction.editReply(createCommandResponseMessage('Processing', 'Selection received, please wait...'));

		const selected = collection.values[0];

		if(selected === DiscordClass.EMOJI.random)
		{
			topReaction = [...topReactions.values()][random(CustomConstants.numbers.ZERO, topReactions.size - CustomConstants.numbers.ONE)];
		}
		else
		{
			topReaction = topReactions.get(selected) as MessageReaction;
		}
	}
	else
	{
		[topReaction] = topReactions.values();
	}

	if(isNullOrUndefined(topReaction))
	{
		return interaction.editReply(createCommandResponseMessage('Fatal error', 'Couldn\'t get the right option. This should never happen and is likely a bug', true));
	}

	const topEmoji = topReaction.emoji.toString();

	await topReaction.users.fetch();

	const usersCollection = topReaction.users.cache;

	usersCollection.delete(discord.client.user?.id as string);

	return setNext(interaction, campaignIdArg, topEmoji, [...usersCollection.values()], unavailableUsers);
};

async function setNext(interaction: TInteractionElementsNotNull<CommandInteraction>, channelId: string, topEmoji: string, available: User[], unavailable: Set<string>): Promise<unknown>
{
	const poll = storage.getPoll(interaction.guildId, channelId);

	if(isNullOrUndefined(poll))
	{
		return interaction.editReply(createCommandResponseMessage('Couldn\'t find poll', 'Couldn\'t get poll information from the database', true));
	}

	const time = poll.get(topEmoji);

	if(isNullOrUndefined(time))
	{
		return interaction.editReply(createCommandResponseMessage('Couldn\'t find date and time', `Couldn't get the date and time that corresponds to ${topEmoji}`, true));
	}

	await deleteCampaignMessage(interaction.guildId, channelId, false, true);

	const saveResult = await storage.setNext(interaction.guildId, channelId, time, available, unavailable, false);

	if(saveResult instanceof Error)
	{
		await storage.cancelNext(interaction.guildId, channelId);

		return interaction.editReply(createCommandResponseMessage('Couldn\'t update database', saveResult.message, true));
	}

	const messageResult = await discord.createOrUpdateNextEmbed(interaction.guild, channelId, true);

	if(messageResult instanceof Error)
	{
		await storage.cancelNext(interaction.guildId, channelId);
		await deleteCampaignMessage(interaction.guildId, channelId);

		return interaction.editReply(createCommandResponseMessage('Couldn\'t create message', messageResult.message, true));
	}

	return interaction.editReply(createCommandResponseMessage('Closed poll', 'Successfully set next session date and time'));
}

export const command = new Command(path, handler);
