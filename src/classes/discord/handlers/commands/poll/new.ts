import type { DateTime } from 'luxon';

import { discord, DiscordClass } from '../../..';
import type { TCommandHandler } from '../../../command';
import { Command } from '../../../command';
import type { TStoragePoll } from '../../../../storage';
import { storage } from '../../../../storage';
import { isNullOrUndefined } from '../../../../../helpers/utilities';
import
{
	createCommandResponseMessage,
	deleteCampaignMessage,
	isInPast,
	parseDate,
} from '../../../helpers';
import { command as setNextCommand } from '../next/set';
import { constants } from '../../../../../helpers/constants';

const path: string = 'poll.new';

const handler: TCommandHandler = async (interaction): Promise<unknown> =>
{
	await interaction.deferReply({ ephemeral: true });

	const campaignIdArg = interaction.options.getString('id');
	const timestampsArg = interaction.options.getString('timestamps');

	if(isNullOrUndefined(campaignIdArg))
	{
		return interaction.editReply(createCommandResponseMessage('Missing parameter', 'You have not provided a campaign id', true));
	}

	if(isNullOrUndefined(timestampsArg))
	{
		return interaction.editReply(createCommandResponseMessage('Missing parameter', 'You have not provided any timestamps', true));
	}

	if(!storage.doesCampaignExist(interaction.guildId, campaignIdArg))
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t find campaign', `Could not find a campaign with the id \`${campaignIdArg}\``, true));
	}

	if(!storage.isUserCampaignLeader(interaction.guildId, campaignIdArg, interaction.user.id))
	{
		return interaction.reply(createCommandResponseMessage('No permission', 'You are not the leader of this campaign', true));
	}

	const timestampSections: string[] = timestampsArg
		.split('|')
		.map((timestamp) =>
		{
			return timestamp.trim();
		})
		.filter((timestamp: string) =>
		{
			return !isNullOrUndefined(timestamp);
		});

	const pollDates: DateTime[] = [];
	const errors: string[] = [];

	for(const timestamp of timestampSections)
	{
		const date: DateTime = parseDate(timestamp);

		if(!date.isValid)
		{
			errors.push(`\`${timestamp}\` [Invalid format]`);

			continue;
		}

		if(isInPast(date))
		{
			errors.push(`\`${timestamp}\` [In the past]`);

			continue;
		}

		pollDates.push(date);
	}

	if(errors.length > constants.numbers.ZERO)
	{
		let message = `There ${errors.length === constants.numbers.ONE ? 'is' : 'are'} ${errors.length} invalid timestamp${errors.length === constants.numbers.ONE ? '' : 's'}, which are as follows:\n`;

		message += errors
			.join('\n');

		return interaction.editReply(createCommandResponseMessage('Invalid timestamps', message, true));
	}

	if(pollDates.length <= constants.numbers.ZERO)
	{
		return interaction.editReply(createCommandResponseMessage('No timestamps', 'You have not provided any timestamps', true));
	}

	if(pollDates.length > discord.maxPollOptions)
	{
		let messageText: string = 'You are trying to add too many options to the poll\n\n';
		messageText += `There is a limit of \`${discord.maxPollOptions}\` options, and you tried to add \`${pollDates.length}\``;
		messageText += 'Try adding fewer options and try again';

		return interaction.editReply(createCommandResponseMessage('Too many options', messageText, true));
	}

	if(pollDates.length <= constants.numbers.ONE)
	{
		const nextCommandString: string = setNextCommand.path.split('.').join(' ');

		return interaction.editReply(createCommandResponseMessage(
			'Single date',
			`You've only supplied 1 date. Use \`/${nextCommandString}\` to directly set the next session date, or supply more dates to create a poll`,
			true,
		));
	}

	const sortedPoll: DateTime[] = pollDates.sort((a, b) =>
	{
		return a.toMillis() - b.toMillis();
	});

	await deleteCampaignMessage(interaction.guildId, campaignIdArg);

	const poll: TStoragePoll = new Map();
	const emojiSet = [...DiscordClass.EMOJI.reaction.values()];

	for(let i = 0; i < sortedPoll.length; i++)
	{
		const date = sortedPoll[i];

		poll.set(emojiSet[i], date);
	}

	const databaseResult = await storage.setPoll(interaction.guildId, campaignIdArg, poll, true);

	if(databaseResult instanceof Error)
	{
		await storage.cancelPoll(interaction.guildId, campaignIdArg);

		return interaction.editReply(createCommandResponseMessage('Couldn\'t update database', databaseResult.message, true));
	}

	const messageResult = await discord.createOrUpdatePollEmbed(interaction.guild, campaignIdArg, true);

	if(messageResult instanceof Error)
	{
		await storage.cancelPoll(interaction.guildId, campaignIdArg);
		await deleteCampaignMessage(interaction.guildId, campaignIdArg);

		return interaction.editReply(createCommandResponseMessage('Couldn\'t create message', messageResult.message, true));
	}

	return interaction.editReply(createCommandResponseMessage('Poll created', `Successfully created a poll for the campaign with id \`${campaignIdArg}\``));
};

export const command = new Command(path, handler);
