import type { TCommandHandler } from '../../../command';
import { Command } from '../../../command';
import { CampaignStatus, storage } from '../../../../storage';
import { isNullOrUndefined } from '../../../../../helpers/utilities';
import { createCommandResponseMessage, deleteCampaignMessage } from '../../../helpers';

const path: string = 'poll.cancel';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const campaignIdArg = interaction.options.getString('id');

	if(isNullOrUndefined(campaignIdArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You have not provided a campaign id', true));
	}

	if(!storage.doesCampaignExist(interaction.guildId, campaignIdArg))
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t find campaign', `Could not find a campaign with the id \`${campaignIdArg}\``, true));
	}

	if(!storage.isUserCampaignLeader(interaction.guildId, campaignIdArg, interaction.user.id))
	{
		return interaction.reply(createCommandResponseMessage('No permission', 'You are not the leader of this campaign', true));
	}

	const campaignInfo = storage.getCampaignInfo(interaction.guildId, campaignIdArg);

	if(isNullOrUndefined(campaignInfo))
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t find campaign', 'Could not get campaign information from the database', true));
	}

	if(campaignInfo.status !== CampaignStatus.VOTING)
	{
		return interaction.reply(createCommandResponseMessage('Not voting', `The campaign with id \`${campaignIdArg}\` doesn't have a poll`, true));
	}

	await deleteCampaignMessage(interaction.guildId, campaignIdArg, false);

	const saveResult = await storage.cancelPoll(interaction.guildId, campaignIdArg, true);

	if(saveResult instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t update database', saveResult.message, true));
	}

	return interaction.reply(createCommandResponseMessage('Poll cancelled', 'Successfully cancelled the poll'));
};

export const command = new Command(path, handler);
