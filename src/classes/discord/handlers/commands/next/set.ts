import type { TCommandHandler } from '../../../command';
import { Command } from '../../../command';
import { CampaignStatus, storage } from '../../../../storage';
import { isNullOrUndefined } from '../../../../../helpers/utilities';
import
{
	createCommandResponseMessage,
	deleteCampaignMessage,
	isInPast,
	parseDate,
} from '../../../helpers';
import { discord } from '../../..';

const path: string = 'next.set';

const handler: TCommandHandler = async (interaction): Promise<void> =>
{
	const campaignIdArg = interaction.options.getString('id');
	const timeArg = interaction.options.getString('timestamp');

	if(isNullOrUndefined(campaignIdArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You have not provided a campaign id', true));
	}

	if(isNullOrUndefined(timeArg))
	{
		return interaction.reply(createCommandResponseMessage('Missing parameter', 'You have not provided a timestamp', true));
	}

	if(!storage.doesCampaignExist(interaction.guildId, campaignIdArg))
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t find campaign', `Could not find a campaign with the id \`${campaignIdArg}\``, true));
	}

	if(!storage.isUserCampaignLeader(interaction.guildId, campaignIdArg, interaction.user.id))
	{
		return interaction.reply(createCommandResponseMessage('No permission', 'You are not the leader of this campaign', true));
	}

	const dateTime = parseDate(timeArg);

	if(!dateTime.isValid)
	{
		return interaction.reply(createCommandResponseMessage('Incorrect timestamp', 'The provided timestamp is incorrectly formatted', true));
	}

	if(isInPast(dateTime))
	{
		return interaction.reply(createCommandResponseMessage('Past timestamp', 'Cannot set the next session date and time to a time in the past', true));
	}

	const campaignInfo = storage.getCampaignInfo(interaction.guildId, campaignIdArg);

	if(!isNullOrUndefined(campaignInfo) && campaignInfo.status === CampaignStatus.VOTING)
	{
		await deleteCampaignMessage(interaction.guildId, campaignIdArg, false);
	}

	const saveResult = await storage.setNext(interaction.guildId, campaignIdArg, dateTime, undefined, new Set(), false);

	if(saveResult instanceof Error)
	{
		await storage.cancelNext(interaction.guildId, campaignIdArg);

		return interaction.reply(createCommandResponseMessage('Couldn\'t update database', saveResult.message, true));
	}

	const messageResult = await discord.createOrUpdateNextEmbed(interaction.guild, campaignIdArg, true);

	if(messageResult instanceof Error)
	{
		await storage.cancelNext(interaction.guildId, campaignIdArg);
		await deleteCampaignMessage(interaction.guildId, campaignIdArg);

		return interaction.reply(createCommandResponseMessage('Couldn\'t create message', messageResult.message, true));
	}

	return interaction.reply(createCommandResponseMessage('Date and time set', 'Successfully set next session date and time'));
};

export const command = new Command(path, handler);
