import { storage } from '../../../../storage';
import { createCommandResponseMessage } from '../../../helpers';
import type { TButtonHandler } from '../../../button';
import { Button } from '../../../button';
import { discord } from '../../..';

const path: string = 'reminder.join';

const handler: TButtonHandler = async (interaction, campaignId): Promise<void> =>
{
	const resultJoin = await storage.joinNext(interaction.guildId, campaignId, interaction.member.user.id, false);

	if(resultJoin instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t mark as available', resultJoin.message, true));
	}

	const resultUpdate = await discord.createOrUpdateNextEmbed(interaction.guild, campaignId, true);

	if(resultUpdate instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t update database', resultUpdate.message, true));
	}

	return interaction.reply(createCommandResponseMessage('Attending', 'Successfully marked yourself as attending this session'));
};

export const button = new Button(path, handler);
