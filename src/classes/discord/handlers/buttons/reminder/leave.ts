import { storage } from '../../../../storage';
import { createCommandResponseMessage } from '../../../helpers';
import type { TButtonHandler } from '../../../button';
import { Button } from '../../../button';
import { discord } from '../../..';

const path: string = 'reminder.leave';

const handler: TButtonHandler = async (interaction, campaignId): Promise<void> =>
{
	const resultLeave = await storage.leaveNext(interaction.guildId, campaignId, interaction.member.user.id, false);

	if(resultLeave instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t mark as unavailable', resultLeave.message, true));
	}

	const resultUpdate = await discord.createOrUpdateNextEmbed(interaction.guild, campaignId, true);

	if(resultUpdate instanceof Error)
	{
		return interaction.reply(createCommandResponseMessage('Couldn\'t update database', resultUpdate.message, true));
	}

	return interaction.reply(createCommandResponseMessage('Unavailable', 'Successfully marked yourself as unavailable for this session'));
};

export const button = new Button(path, handler);
