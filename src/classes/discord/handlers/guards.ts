import type { Guild, GuildMember, Interaction } from 'discord.js';

import { isNullOrUndefined } from '../../../helpers/utilities';

export interface IInteractionGuildNotNull extends Interaction
{
	guild: Guild;
	guildId: string;
}

export interface IInteractionMemberNotNull extends Interaction
{
	member: GuildMember;
}

export type TInteractionElementsNotNull<T extends Interaction> = IInteractionGuildNotNull & IInteractionMemberNotNull & T;

export function interactionHasGuild<T extends Interaction>(interaction: T): interaction is IInteractionGuildNotNull & T
{
	return !isNullOrUndefined(interaction.guildId) && !isNullOrUndefined(interaction.guild);
}

export function interactionHasMember<T extends Interaction>(interaction: T): interaction is IInteractionMemberNotNull & T
{
	return !isNullOrUndefined(interaction.member);
}
