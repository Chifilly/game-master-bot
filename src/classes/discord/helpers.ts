import type {
	EmbedField,
	EmbedFieldData,
	InteractionReplyOptions,
	Message,
	TextChannel,
} from 'discord.js';
import
{
	MessageActionRow,
	MessageEmbed,
	Permissions,
} from 'discord.js';
import type { DurationUnits } from 'luxon';
import { DateTime, Duration } from 'luxon';

import { discord, DiscordClass } from '.';

import { constants } from '../../helpers/constants';
import { isNullOrUndefined } from '../../helpers/utilities';
import type { IConfigOffsets } from '../config';
import { config } from '../config';
import type { IStorageNext } from '../storage';
import { CampaignStatus, storage } from '../storage';
import { reminderJoinButton, reminderLeaveButton } from './buttons';

export const outputDateFormat: string = 'EEE dd MMM yyyy' as const;

export const outputTimeFormat: string = 'hh:mm a' as const;

export const inputDateTimeFormat: string = 'dd MM yyyy HH:mm Z' as const;

export const reminderUnits: Map<string, string> = new Map([
	['y', 'years'],
	['M', 'months'],
	['w', 'weeks'],
	['d', 'days'],
	['h', 'hours'],
	['m', 'minutes'],
]);

type TPermissionObjectData = [string, string];

type TPermissionObject = [bigint, TPermissionObjectData];

type TPermissionMap = Map<bigint, TPermissionObjectData>;

const permissionViewChannels: TPermissionObject = [Permissions.FLAGS.VIEW_CHANNEL, ['View Channels', 'Required to find the appropriate channels to post reminders and polls into']];
const permissionSendMessages: TPermissionObject = [Permissions.FLAGS.SEND_MESSAGES, ['Send Messages', 'Required to send messages in the reminder and poll channels']];
const permissionManageMessages: TPermissionObject = [Permissions.FLAGS.MANAGE_MESSAGES, ['Manage Messages', 'Required to be able to edit and delete reminder and poll messages']];
const permissionEmbedLinks: TPermissionObject = [Permissions.FLAGS.EMBED_LINKS, ['Embed Links', 'Required to make the reminder and poll messages as they use embeds']];
const permissionReadMessageHistory: TPermissionObject = [Permissions.FLAGS.READ_MESSAGE_HISTORY, ['Read Message History', 'Required to be able to access old messages after a restart']];
const permissionMentionEveryone: TPermissionObject = [Permissions.FLAGS.MENTION_EVERYONE, ['Mention Everyone', 'Required to be able to use `@here` when creating a poll']];
const permissionAddReactions: TPermissionObject = [Permissions.FLAGS.ADD_REACTIONS, ['Add Reactions', 'Required to add reactions on poll messages which are used for tracking votes']];

export const requiredPermissions: TPermissionMap = new Map([
	permissionViewChannels,
	permissionSendMessages,
	permissionManageMessages,
	permissionEmbedLinks,
	permissionReadMessageHistory,
	permissionMentionEveryone,
	permissionAddReactions,
]);

export const requiredReminderChannelPermissions: TPermissionMap = new Map([
	permissionViewChannels,
	permissionSendMessages,
	permissionManageMessages,
	permissionEmbedLinks,
	permissionReadMessageHistory,
]);

export const requiredPollChannelPermissions: TPermissionMap = new Map([
	permissionViewChannels,
	permissionSendMessages,
	permissionManageMessages,
	permissionEmbedLinks,
	permissionReadMessageHistory,
	permissionMentionEveryone,
	permissionAddReactions,
]);

export function isInPast(date: DateTime): boolean
{
	return getTimeDifference(date).as('milliseconds') < constants.numbers.ZERO;
}

export function createMentionFromId(id: string, type: 'member' | 'role'): string
{
	let prefix: string = '';

	switch(type)
	{
		case 'role':
			prefix = '@&';

			break;

		case 'member':
		default:
			prefix = '@';
	}

	return `<${prefix}${id}>`;
}

export function createMentionedMembersListFromNext(next: IStorageNext, available: boolean = true): string[]
{
	const members: string[] = [];

	const list = available ? next.members : next.unavailable;

	for(const memberId of list)
	{
		members.push(createMentionFromId(memberId, 'member'));
	}

	return members;
}

export async function createMentionedMembersListFromId(guildId: string, campaignId: string): Promise<Error | string[]>
{
	const next = storage.getNext(guildId, campaignId);

	if(isNullOrUndefined(next))
	{
		return new Error('Could not get next session information');
	}

	return createMentionedMembersListFromNext(next);
}

export async function buildNextEmbed(guildId: string, campaignId: string): Promise<MessageEmbed | undefined>
{
	const campaign = storage.getCampaignInfo(guildId, campaignId);
	const next = storage.getNext(guildId, campaignId);

	if(isNullOrUndefined(next) || isNullOrUndefined(campaign))
	{
		return;
	}

	const embed = new MessageEmbed()
		.setColor(isInPast(next.time) ? DiscordClass.COLORS.past : DiscordClass.COLORS.future)
		.setTitle(`${campaign.name}`)
		.setDescription('The next session is at the following date and time');

	const fields: EmbedFieldData[] = [];

	for(const offset of config.offsets)
	{
		fields.push(createTimezoneField(offset, next.time));
	}

	embed.addFields(fields);

	const members = createMentionedMembersListFromNext(next);
	const unavailable = createMentionedMembersListFromNext(next, false);

	embed.addField('People attending this session', members.length > constants.numbers.ZERO ? members.join(' ') : 'Nobody', false);
	embed.addField('People unavailable for this session', unavailable.length > constants.numbers.ZERO ? unavailable.join(' ') : 'Nobody', false);

	embed.setFooter(`${DiscordClass.EMOJI.timeFrom} ${getRelativeNextDate(guildId, campaignId)} • ${campaignId}`);

	return embed;
}

export async function buildPollEmbed(guildId: string, campaignId: string): Promise<MessageEmbed[] | undefined>
{
	const campaign = storage.getCampaignInfo(guildId, campaignId);
	const poll = storage.getPoll(guildId, campaignId);

	if(isNullOrUndefined(campaign) || isNullOrUndefined(poll))
	{
		return;
	}

	let messageDescription: string = 'Vote for when you are available for the next session\n';
	messageDescription = `Use the ${DiscordClass.EMOJI.unavailable} reaction if you are unable to make it to any of the dates and times\n\n`;
	messageDescription += 'Note: You should pay attention to the dates and their corresponding reactions, as the reactions aren\'t necessarily in ascending order if the poll has been edited';

	const embeds: MessageEmbed[] = [
		new MessageEmbed()
			.setColor(DiscordClass.COLORS.voting)
			.setTitle(`${campaign.name}`)
			.setDescription(messageDescription)
			.setFooter(campaignId),
	];

	const sortedPoll = [...poll.entries()]
		.sort((a, b) =>
		{
			return a[1].toMillis() - b[1].toMillis();
		});

	for(const [emoji, time] of sortedPoll)
	{
		const fields: EmbedFieldData[] = [];

		const embed = new MessageEmbed()
			.setColor(DiscordClass.COLORS.voting)
			.setDescription('Respond with the appropriate emoji below if you can make this date and time')
			.setTitle(`${emoji}`);

		for(let offsetIndex = 0; offsetIndex < config.offsets.length; offsetIndex++)
		{
			const field: EmbedField = createTimezoneField(config.offsets[offsetIndex], time);

			field.name = `${field.name}`;
			field.value = `${field.value}${offsetIndex === config.offsets.length - constants.numbers.ONE ? '\n\u200B' : ''}`;

			fields.push(field);
		}

		embed.addFields(fields);

		embeds.push(embed);
	}

	return embeds;
}

export function buildMessageHyperlink(guildId: string, channelId: string, messageId: string, linkText: string, hoverText: string = linkText): string
{
	return `[${linkText}](https://discord.com/channels/${guildId}/${channelId}/${messageId} '${hoverText}')`;
}

export async function buildCampaignsEmbed(guildId: string): Promise<MessageEmbed>
{
	const campaigns = storage.getCampaigns(guildId);

	const embed = new MessageEmbed()
		.setColor(DiscordClass.COLORS.future)
		.setTitle('Campaigns');

	const fields: EmbedField[] = [];

	if(isNullOrUndefined(campaigns) || campaigns.size <= constants.numbers.ZERO)
	{
		fields.push({
			name: 'None',
			value: 'There are currently no campaigns created',
			inline: false,
		});
	}
	else
	{
		const nameField: EmbedField = {
			name: 'Name and Leader',
			value: '',
			inline: true,
		};

		const idField: EmbedField = {
			name: 'Id',
			value: '',
			inline: true,
		};

		const statusField: EmbedField = {
			name: 'Status',
			value: '',
			inline: true,
		};

		for(const [campaignId, campaign] of campaigns)
		{
			let status: string = 'None';

			if(!isNullOrUndefined(campaign.message))
			{
				switch(campaign.status)
				{
					case CampaignStatus.SET:
						status = buildMessageHyperlink(guildId, campaign.message.channel, campaign.message.id, 'Date and Time Set', 'Go to reminder message');

						break;

					case CampaignStatus.VOTING:
						status = buildMessageHyperlink(guildId, campaign.message.channel, campaign.message.id, 'Currently Voting', 'Go to poll');

						break;

					case CampaignStatus.NONE:
					default:
						status = 'None';
				}
			}

			const leaders = [...campaign.leaders.values()].map((userId) =>
			{
				return createMentionFromId(userId, 'member');
			});

			nameField.value += `\n**${campaign.name.trim()}** ${leaders.join(' ')}`;
			idField.value += `\n\`${campaignId.trim()}\``;
			statusField.value += `\n${status.trim()}`;
		}

		fields.push(nameField, idField, statusField);
	}

	embed.addFields(fields);

	return embed;
}

export function createTimezoneField(offset: IConfigOffsets, date: DateTime, inline: boolean = true): EmbedField
{
	const formattedDate = formatDate(date, offset.hours);

	return {
		name: `${offset.text}`,
		value: `${DiscordClass.EMOJI.date} ${formattedDate.date}\n${DiscordClass.EMOJI.time} ${formattedDate.time}\n${DiscordClass.EMOJI.offset} ${formattedDate.offset}`,
		inline,
	};
}

export function getTimeDifference(firstDate: DateTime, secondDate: DateTime = DateTime.utc(), unit: DurationUnits = 'seconds'): Duration
{
	return firstDate.diff(secondDate, unit);
}

export function formatDate(date: DateTime, offset: number): { date: string; time: string; offset: string }
{
	const offsetDate: DateTime = date.plus({ hours: offset });
	const offsetText: string = `${offset < constants.numbers.ZERO ? '-' : '+'}${offset}`;

	return {
		date: offsetDate.toFormat(outputDateFormat),
		time: offsetDate.toFormat(outputTimeFormat),
		offset: `GMT${offsetText}`,
	};
}

export function parseDate(timestamp: string): DateTime
{
	return DateTime.fromFormat(timestamp, inputDateTimeFormat).toUTC();
}

export async function getCampaignMessage(guildId: string, campaignId: string): Promise<Message | undefined>
{
	const messageData = storage.getCampaignMessage(guildId, campaignId);
	const guild = discord.client.guilds.cache.get(guildId);

	if(
		isNullOrUndefined(messageData) ||
		isNullOrUndefined(guild)
	)
	{
		return;
	}

	const channel = guild.channels.cache.get(messageData.channel) as TextChannel;

	if(channel.partial)
	{
		await channel.fetch();
	}

	if(isNullOrUndefined(channel))
	{
		return;
	}

	let message = channel.messages.cache.get(messageData.id);

	if(isNullOrUndefined(message))
	{
		await channel.messages.fetch();
	}

	message = channel.messages.cache.get(messageData.id);

	return message;
}

export async function deleteCampaignMessage(guildId: string, campaignId: string, save: boolean = true, permanent: boolean = true): Promise<Error | undefined>
{
	const message = await getCampaignMessage(guildId, campaignId);

	if(!isNullOrUndefined(message))
	{
		await message.delete();
	}

	if(permanent)
	{
		return storage.deleteCampaignMessage(guildId, campaignId, save);
	}
}

export function parseReminderString(reminder: string): Duration
{
	const number: number = Number(reminder.slice(constants.numbers.ZERO, -constants.numbers.ONE));
	const unit: string = reminder.slice(reminder.length - constants.numbers.ONE);

	const diffItem = reminderUnits.get(unit);

	let errorString = '';

	if(!Number.isNaN(number))
	{
		if(!isNullOrUndefined(diffItem))
		{
			const unitObject = { [diffItem]: number };

			return Duration.fromObject(unitObject);
		}

		errorString = `${unit} is an invalid reminder time unit`;
	}
	else
	{
		errorString = `${number} is not a number`;
	}

	return Duration.invalid(errorString);
}

export function getRelativeNextDate(guildId: string, campaignId: string): string
{
	const campaign = storage.getNext(guildId, campaignId);

	if(isNullOrUndefined(campaign))
	{
		return 'unknown';
	}

	const diff = getTimeDifference(campaign.time).as('seconds');

	let { time } = campaign;

	if(diff > constants.numbers.ZERO)
	{
		time = campaign.time.plus({ minutes: 1 });
	}

	let relative: string | null = '';

	if(diff < DiscordClass.REMINDER_COMPARISON_BUFFER && diff > -DiscordClass.REMINDER_COMPARISON_BUFFER)
	{
		relative = 'now';
	}
	else
	{
		relative = time.toRelative({ style: 'long', unit: ['years', 'months', 'weeks', 'days', 'hours', 'minutes'] });
	}

	if(isNullOrUndefined(relative))
	{
		return 'unknown';
	}

	return relative;
}

export function createCommandResponseMessage(title: string, message: string, error: boolean = false, ephemeral: boolean = true): InteractionReplyOptions
{
	return {
		embeds: [discord.createInfoEmbed(title, message, error)],
		ephemeral,
		components: [],
	};
}

export function buildReminderActions(campaignId: string): MessageActionRow[]
{
	const row = new MessageActionRow()
		.addComponents(
			reminderJoinButton(campaignId),
			reminderLeaveButton(campaignId),
		);

	return [row];
}
