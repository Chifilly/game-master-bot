import { Constants } from 'discord.js';
import type { ApplicationCommandData } from 'discord.js';

import { AuthorityLevel } from './command';
import type { Command } from './command';
import { commands } from './handlers/index';

export const permissionMap: Map<string, AuthorityLevel> = new Map([
	[
		'campaigns',
		AuthorityLevel.PUBLIC,
	],
	[
		'poll',
		AuthorityLevel.LEADER,
	],
	[
		'next',
		AuthorityLevel.LEADER,
	],
	[
		'campaign',
		AuthorityLevel.LEADER,
	],
	[
		'settings',
		AuthorityLevel.MANAGER,
	],
	[
		'setup',
		AuthorityLevel.OWNER,
	],
]);

export const slashCommands: ApplicationCommandData[] = [
	// ========================================================================
	// = CAMPAIGNS
	// ========================================================================
	{
		name: 'campaigns',
		description: 'List all currently created campaigns and their status',
	},

	// ========================================================================
	// = POLL
	// ========================================================================
	{
		name: 'poll',
		description: 'Manage polls',
		defaultPermission: false,
		options: [
			// ================================================================
			// = POLL.NEW
			// ================================================================
			{
				name: 'new',
				description: 'Create a new poll for a campaign',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
				options: [
					{
						name: 'id',
						description: 'The id of the campaign you want to create a poll for',
						type: Constants.ApplicationCommandOptionTypes.STRING,
						required: true,
					},
					{
						name: 'timestamps',
						description: 'A | separated list of timestamps to use in the poll',
						type: Constants.ApplicationCommandOptionTypes.STRING,
						required: true,
					},
				],
			},

			// ================================================================
			// = POLL.CLOSE
			// ================================================================
			{
				name: 'close',
				description: 'Close the poll for a campaign and set the next session date and time',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
				options: [
					{
						name: 'id',
						description: 'The id of the campaign you want to close the poll for',
						type: Constants.ApplicationCommandOptionTypes.STRING,
						required: true,
					},
				],
			},

			// ================================================================
			// = POLL.CANCEL
			// ================================================================
			{
				name: 'cancel',
				description: 'Cancel the poll for a campaign',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
				options: [
					{
						name: 'id',
						description: 'The id of the campaign you want to cancel the poll for',
						type: Constants.ApplicationCommandOptionTypes.STRING,
						required: true,
					},
				],
			},

			// ================================================================
			// = POLL.EDIT
			// ================================================================
			{
				name: 'edit',
				description: 'Edit the poll for a campaign',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND_GROUP,
				options: [
					// ========================================================
					// = POLL.EDIT.ADD
					// ========================================================
					{
						name: 'add',
						description: 'Add dates and times to a poll for a campaign',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
						options: [
							{
								name: 'id',
								description: 'The id of the campaign you want to add dates and times to the poll of',
								type: Constants.ApplicationCommandOptionTypes.STRING,
								required: true,
							},
							{
								name: 'timestamps',
								description: 'A | separated list of timestamps to add to the poll',
								type: Constants.ApplicationCommandOptionTypes.STRING,
								required: true,
							},
						],
					},

					// ========================================================
					// = POLL.EDIT.CHANGE
					// ========================================================
					{
						name: 'change',
						description: 'Change the date and time of a poll option for a campaign',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
						options: [
							{
								name: 'id',
								description: 'The id of the campaign you want to change the date and time of in its poll',
								type: Constants.ApplicationCommandOptionTypes.STRING,
								required: true,
							},
							{
								name: 'emoji',
								description: 'The emoji identifier of the poll option you want to change',
								type: Constants.ApplicationCommandOptionTypes.STRING,
								required: true,
							},
							{
								name: 'timestamp',
								description: 'A timestamp to change the options date and time to',
								type: Constants.ApplicationCommandOptionTypes.STRING,
								required: true,
							},
						],
					},

					// ========================================================
					// = POLL.EDIT.REMOVE
					// ========================================================
					{
						name: 'remove',
						description: 'Remove a poll option for a campaign',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
						options: [
							{
								name: 'id',
								description: 'The id of the campaign you want to remove a poll option of',
								type: Constants.ApplicationCommandOptionTypes.STRING,
								required: true,
							},
							{
								name: 'emoji',
								description: 'The emoji identifier of the poll option you want to remove',
								type: Constants.ApplicationCommandOptionTypes.STRING,
								required: true,
							},
						],
					},
				],
			},
		],
	},

	// ========================================================================
	// = NEXT
	// ========================================================================
	{
		name: 'next',
		description: 'Settings relating to the next campaign session',
		defaultPermission: false,
		options: [
			{
				// ============================================================
				// = NEXT.SET
				// ============================================================
				name: 'set',
				description: 'Set the date and time of the next session of a campaign',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
				options: [
					{
						name: 'id',
						description: 'The id of the campaign you want to set the date and time for',
						type: Constants.ApplicationCommandOptionTypes.STRING,
						required: true,
					},
					{
						name: 'timestamp',
						description: 'The timestamp you want to set it to',
						type: Constants.ApplicationCommandOptionTypes.STRING,
						required: true,
					},
				],
			},
			{
				// ============================================================
				// = NEXT.CANCEL
				// ============================================================
				name: 'cancel',
				description: 'Cancel the scheduled session of a campaign',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
				options: [
					{
						name: 'id',
						description: 'The id of the campaign you want to cancel the session for',
						type: Constants.ApplicationCommandOptionTypes.STRING,
						required: true,
					},
				],
			},
		],
	},

	// ========================================================================
	// = CAMPAIGN
	// ========================================================================
	{
		name: 'campaign',
		description: 'Manage campaigns',
		defaultPermission: false,
		options: [
			// ================================================================
			// = CAMPAIGN.NEW
			// ================================================================
			{
				name: 'new',
				description: 'Create a new campaign',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
				options: [
					{
						name: 'name',
						description: 'The name of the campaign',
						type: Constants.ApplicationCommandOptionTypes.STRING,
						required: true,
					},
				],
			},

			// ================================================================
			// = CAMPAIGN.RENAME
			// ================================================================
			{
				name: 'rename',
				description: 'Rename a campaign',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
				options: [
					{
						name: 'id',
						description: 'The id of the campaign you want to rename',
						type: Constants.ApplicationCommandOptionTypes.STRING,
						required: true,
					},
					{
						name: 'name',
						description: 'The new name of the campaign',
						type: Constants.ApplicationCommandOptionTypes.STRING,
						required: true,
					},
				],
			},

			// ================================================================
			// = CAMPAIGN.DELETE
			// ================================================================
			{
				name: 'delete',
				description: 'Delete a campaign',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
				options: [
					{
						name: 'id',
						description: 'The id of the campaign you want to delete',
						type: Constants.ApplicationCommandOptionTypes.STRING,
						required: true,
					},
				],
			},
		],
	},

	// ========================================================================
	// = SETTINGS
	// ========================================================================
	{
		name: 'settings',
		description: 'Manage settings',
		defaultPermission: false,
		options: [
			// ================================================================
			// = SETUP.LEADERS
			// ================================================================
			{
				name: 'leaders',
				description: 'Settings relating to campaign leaders',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND_GROUP,
				options: [
					// ========================================================
					// = SETUP.LEADERS.ADD
					// ========================================================
					{
						name: 'add',
						description: 'Add a role to be treated as a leader',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
						options: [
							{
								name: 'role',
								description: 'The role to add',
								type: Constants.ApplicationCommandOptionTypes.ROLE,
								required: true,
							},
						],
					},

					// ========================================================
					// = SETUP.LEADERS.REMOVE
					// ========================================================
					{
						name: 'remove',
						description: 'Remove a role from being a leader',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
						options: [
							{
								name: 'role',
								description: 'The role to remove',
								type: Constants.ApplicationCommandOptionTypes.ROLE,
								required: true,
							},
						],
					},

					// ========================================================
					// = SETUP.LEADERS.LIST
					// ========================================================
					{
						name: 'list',
						description: 'List the currently assigned leader roles',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
					},
				],
			},

			// ================================================================
			// = SETTINGS.POLLS
			// ================================================================
			{
				name: 'polls',
				description: 'Settings relating to polls',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND_GROUP,
				options: [
					// ========================================================
					// = SETTINGS.POLLS.CHANNEL
					// ========================================================
					{
						name: 'channel',
						description: 'Set the channel you\'re executing this command in as the channel polls will be sent to',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
						options: [
							{
								name: 'channel',
								description: 'The channel to send polls to',
								type: Constants.ApplicationCommandOptionTypes.CHANNEL,
								required: true,
							},
						],
					},
				],
			},

			// ================================================================
			// = SETTINGS.REMINDERS
			// ================================================================
			{
				name: 'reminders',
				description: 'Settings relating to reminders',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND_GROUP,
				options: [
					// ========================================================
					// = SETTINGS.REMINDERS.CHANNEL
					// ========================================================
					{
						name: 'channel',
						description: 'Set the channel you\'re executing this command in as the channel reminders will be sent to',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
						options: [
							{
								name: 'channel',
								description: 'The channel to send reminders to',
								type: Constants.ApplicationCommandOptionTypes.CHANNEL,
								required: true,
							},
						],
					},

					// ========================================================
					// = SETTINGS.REMINDERS.SET
					// ========================================================
					{
						name: 'set',
						description: 'Set the reminder times',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
						options: [
							{
								name: 'times',
								description: 'A space-separated list of times',
								type: Constants.ApplicationCommandOptionTypes.STRING,
								required: true,
							},
						],
					},

					// ========================================================
					// = SETTINGS.REMINDERS.LIST
					// ========================================================
					{
						name: 'list',
						description: 'List the currently set reminder times',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
					},

					// ========================================================
					// = SETTINGS.REMINDERS.ENABLED
					// ========================================================
					{
						name: 'enabled',
						description: 'Enable or disable reminders',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
						options: [
							{
								name: 'enabled',
								description: 'Whether to have reminders enabled',
								type: Constants.ApplicationCommandOptionTypes.BOOLEAN,
								required: true,
							},
						],
					},
				],
			},
		],
	},

	// ========================================================================
	// = SETUP
	// ========================================================================
	{
		name: 'setup',
		description: 'Manage setup-related things [SERVER OWNER ONLY]',
		defaultPermission: false,
		options: [
			// ================================================================
			// = SETUP.MANAGERS
			// ================================================================
			{
				name: 'managers',
				description: 'Settings relating to bot managers',
				type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND_GROUP,
				options: [
					// ========================================================
					// = SETUP.MANAGERS.ADD
					// ========================================================
					{
						name: 'add',
						description: 'Add a role to be treated as a manager',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
						options: [
							{
								name: 'role',
								description: 'The role to add',
								type: Constants.ApplicationCommandOptionTypes.ROLE,
								required: true,
							},
						],
					},

					// ========================================================
					// = SETUP.MANAGERS.REMOVE
					// ========================================================
					{
						name: 'remove',
						description: 'Remove a role from being a manager',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
						options: [
							{
								name: 'role',
								description: 'The role to remove',
								type: Constants.ApplicationCommandOptionTypes.ROLE,
								required: true,
							},
						],
					},

					// ========================================================
					// = SETUP.MANAGERS.LIST
					// ========================================================
					{
						name: 'list',
						description: 'List the currently assigned manager roles',
						type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND,
					},
				],
			},
		],
	},
];

// =====

export const commandMap: Map<string, Command> = new Map();
for(const command of commands)
{
	commandMap.set(command.path, command);
}
