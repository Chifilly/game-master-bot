export interface IConfig
{
	token: string;
	offsets: IConfigOffsets[];
}

export interface IConfigFile
{
	token: string;
	offsets: IConfigOffsets[];
}

export interface IConfigOffsets
{
	hours: number;
	text: string;
}
