import { promises as fs } from 'fs';

import { parse } from 'yaml';

import type { IConfig, IConfigFile, IConfigOffsets } from './interface';
import { logger, label } from '../logger';
import { isNullOrUndefined } from '../../helpers/utilities';
import { constants } from '../../helpers/constants';

export type { IConfig, IConfigFile, IConfigOffsets } from './interface';

class ConfigClass
{
	private _config: IConfig = {
		token: '',
		offsets: [],
	};

	public async init(): Promise<void>
	{
		logger.info('Loading config file', label.CONFIG);

		return fs.readFile('./config.yml', { encoding: 'utf-8' })
			.then((contents: string) =>
			{
				logger.info('Parsing config file', label.CONFIG);

				let config: IConfigFile | null = null;

				try
				{
					config = parse(contents);
				}
				catch(error)
				{
					throw new Error(`Error parsing config: ${error}`);
				}

				if(!isNullOrUndefined(config))
				{
					return this._validateConfig(config)
						.then(async () =>
						{
							this._config = Object.assign(config, {});

							logger.info('Successfully loaded config', label.CONFIG);

							return Promise.resolve();
						});
				}
			});
	}

	public get token(): string
	{
		return this._config.token;
	}

	public get offsets(): IConfigOffsets[]
	{
		return this._config.offsets;
	}

	private async _validateConfig(config: IConfigFile): Promise<void>
	{
		logger.info('Validating config file', label.CONFIG);

		return new Promise<void>((resolve, reject) =>
		{
			let errors: number = 0;

			if(isNullOrUndefined(config.token))
			{
				logger.error('Validation error: Missing \'token\' value', label.CONFIG);

				errors++;
			}

			if(isNullOrUndefined(config.offsets) || config.offsets.length <= constants.numbers.ZERO)
			{
				logger.error('Validation error: Missing or empty \'offsets\' array', label.CONFIG);

				errors++;
			}

			if(config.offsets.length > constants.numbers.MAX_FIELDS_PER_EMBED)
			{
				logger.error(`Validation error: Too many options present in 'offsets' array. The limit is ${constants.numbers.MAX_FIELDS_PER_EMBED} and there are ${config.offsets.length}`);

				errors++;
			}

			for(let i = 0; i < config.offsets.length; i++)
			{
				const offset = config.offsets[i];

				if(isNullOrUndefined(offset.hours))
				{
					logger.error(`Validation error: Offset at index ${i} in 'offset' array is missing 'hours' value`, label.CONFIG);

					errors++;
				}

				if(isNullOrUndefined(offset.text))
				{
					logger.error(`Validation error: Offset at index ${i} in 'offset' array is missing 'text' value`, label.CONFIG);

					errors++;
				}

				if(typeof offset.hours !== 'number')
				{
					logger.error(`Validation error: Offset at index ${i} in 'offset' array 'hour' value is not a valid number`, label.CONFIG);

					errors++;
				}
			}

			if(errors > constants.numbers.ZERO)
			{
				logger.error(
					`Config failed validation. There ${errors > constants.numbers.ONE ? 'were' : 'was'} ${errors} error${errors > constants.numbers.ONE ? 's' : ''}. Please check previous error messages for more information`,
					label.CONFIG,
				);

				return reject();
			}

			logger.info('Config successfully validated', label.CONFIG);

			return resolve();
		});
	}
}

export const config = new ConfigClass();
