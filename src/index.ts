import { exit } from 'process';

import { logger, label } from './classes/logger';
import { storage } from './classes/storage';
import { config } from './classes/config';
import { discord } from './classes/discord';
import { cron } from './classes/cron';

(async () =>
{
	try
	{
		logger.info('Starting initialisation', label.INIT);

		await Promise.all([storage.init(), config.init()]);
		await discord.init();
		await cron.init();

		logger.info('Completed initialisation', label.INIT);
	}
	catch(e: unknown)
	{
		throw new Error('Error in initialisation');
	}
})()
	.catch((e) =>
	{
		logger.error(`${e.message}`, label.INIT);
		logger.error('FATAL ERROR! See any preceding error messages for the cause', label.INIT);

		exit();
	});
