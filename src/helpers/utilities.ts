import { constants } from './constants';

export function random(min: number, max: number): number
{
	return Math.floor(Math.random() * (max - min + constants.numbers.ONE) + min);
}

export function isNullOrUndefined(item: unknown): item is null | undefined
{
	return item === null || item === undefined;
}

export function isNullOrUndefinedOrEmpty(item: unknown): item is '' | null | undefined
{
	return isNullOrUndefined(item) || item === '';
}
