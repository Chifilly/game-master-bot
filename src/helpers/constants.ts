interface IConstants
{
	numbers: typeof ConstantNumbers;
}

enum ConstantNumbers
{
	ZERO = 0,
	ONE = 1,
	DOUBLE_DIGIT_THRESHOLD = 10,
	MAX_FIELDS_PER_EMBED = 25,
	MAX_EMBEDS_PER_MESSAGE = 10,
	NANOID_LENGTH = 5,
}

export const constants: IConstants = {
	numbers: ConstantNumbers,
};
