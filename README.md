# Game Master Bot

Game Master Bot is a custom Discord bot I created that makes it easier for all members of a DnD campaign I'm part of to vote on and keep track of the date of the next session.  

It allows you to create a poll that people in the campaign can vote on using reactions in order to communicate their availability, and can automatically pick the winning date and time, keep track of it, and output that date and time to help people keep track of it.  

I decided to make it open-source so other people can make use of it as-is, or tweak the code to their liking.  

## Important Notes

- In order to use or develop the bot, you will need to have [Node](https://nodejs.org/) installed
- **Requires** Node version `16.6.x` or higher  

## Setup and Usage

_Note: A more extensive wiki is planned_  
_The bot works mostly as it did before v5, however the setup process once it's on the server is slightly different_  

### Running the Bot

1. Go to the [Releases](https://gitlab.com/Chifilly/game-master-bot/-/releases) page and download the latest release  
1. Extract the zip to a place of your choosing  
1. Execute `npm install --production`  
1. Open `config.yml` and put your Discord bot token next to `token:`  
_Make sure there is a space after the colon when pasting your token; eg. `token: abcdef...` not `token:abcdef...`_  
1. Complete the rest of `config.yml` to your liking  
1. Run `index.js` with Node in whichever way you wish  
_Make sure you pass the `NODE_ENV=production` environment variable_  
_Although not strictly necessary, it's better to be sure (I am but one person and I could have made a mistake)_  

### Setting Up the Bot

_Note: Only the server owner can use the `/setup` commands_  
_Note: The server owner always has access to all commands_  

1. (optional) As the server owner, use the `/setup managers` set of commands to manage the manager roles of the bot  
_Managers can use the more privileged commands, which are: `/settings`, `/next`, `/poll` and `/campaign`_  
1. (optional) Use the `/settings leaders` set of commands to manage the leader roles of the bot  
_Leaders can use the campaign-specific commands, which are: `/next`, `/poll` and `/campaign`_  
1. Use the `/settings polls channel` and `/settings reminders channel` commands to set the channels that poll and reminder messages will be sent to  
1. Use the `/campaign` set of commands to manage the campaigns on the server. Each campaign has its own individual poll and session date and time  

## Commands

The bot uses Discord's built-in slash-command system (aka application commands)
so all you need to do is type `/` into the chat box and it will automatically list all
commands available, complete with tab-completion of parameters. However, a basic description of the top-level commands are listed below. A wiki is planned that will have more extensive information  

- `/campaigns`: **[Everyone]** This command will list all the campaigns currently created, will show you their ids, and allow you to jump to a campaign message if it currently has an active poll or a session date and time set  
- `/campaign`: **[Owner, Managers and Leaders Only]** This set of commands will allow you to create, rename and delete campaigns  
- `/poll`: **[Owner, Managers and Leaders Only]** This set of commands will allow you to create, cancel, close and edit a poll for a campaign (the same way it was done previously, albeit targeting a campaign as well as some renamed commands)  
- `/next`: **[Owner, Managers and Leaders Only]** This set of commands will allow you to set the next session time and date, as well as allow you to cancel a session (the same way it was done previously, albeit targeting a campaign as well as some renamed commands)  
- `/settings`: **[Owner and Managers Only]** This set of commands will allow you to set message channels for polls and reminders, assign, remove or list leader roles, as well as enable/disable, set or list the times that the reminder message will ping those involved in the campaign  
- `/setup`: **[Owner Only]** This set of commands will allow you to add, remove or list the manager roles  

## Development

1. Clone the repository into whatever directory you like  
1. Execute `npm install` in that directory to install all the dependencies  
1. Copy `config.yml` from `src/assets` into the root of the project  
1. Fill in the copied `config.yml` the same way as outlined in steps 4 and 5 in the [`Running the Bot`](#running-the-bot) section  
_I recommend using a separate bot account for the development version of the bot_  
1. Execute `npm run serve` to build and run the bot for development, which watches the files for changes and restarts the bot automatically  

## License

See the [LICENSE](https://gitlab.com/Chifilly/game-master-bot/-/blob/master/LICENSE.md) file for license rights and limitations (MIT)  
