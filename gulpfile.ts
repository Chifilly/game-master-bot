import
{
	dest,
	parallel,
	series,
	src,
	task,
} from 'gulp';
import { createProject as createTSProject } from 'gulp-typescript';
import { init as initSourceMaps, write as writeSourceMaps } from 'gulp-sourcemaps';
import del from 'del';
import nodemon from 'gulp-nodemon';
import zip from 'gulp-zip';
import rename from 'gulp-rename';
import cached from 'gulp-cached';
import uglify from 'gulp-uglify-es';

const tsProject = createTSProject('tsconfig.json');

const outputLocations = {
	development: './out',
	production: './dist',
} as const;

const nodemonChangeDelay = 500;

// Compile TypeScript to JavaScript into the development output folder with sourcemaps
function devCompile()
{
	const results = tsProject.src()
		.pipe(cached(outputLocations.development))
		.pipe(
			initSourceMaps(),
		)
		.pipe(
			tsProject(),
		);

	return results.js
		.pipe(
			writeSourceMaps('.', { includeContent: false, sourceRoot: '../src' }),
		)
		.pipe(
			dest(outputLocations.development),
		);
}

// Compile TypeScript to JavaScript into the production output folder
function prodCompile()
{
	const results = tsProject.src()
		.pipe(
			tsProject(),
		);

	return results.js
		.pipe(uglify(
			{
				toplevel: true,
			},
		))
		.pipe(
			dest(outputLocations.production),
		);
}

// Deletes the development output folder
async function devEmpty()
{
	return del([
		outputLocations.development,
	]);
}

// Deletes the production output folder
async function prodEmpty()
{
	return del([
		outputLocations.production,
	]);
}

async function devClean()
{
	return del([
		`${outputLocations.development}/**/interface.js`,
		`${outputLocations.development}/**/interface.js.map`,
		`${outputLocations.development}/**/*.interface.js`,
		`${outputLocations.development}/**/*.interface.js.map`,
	]);
}

async function prodClean()
{
	return del([
		`${outputLocations.production}/**/interface.js`,
		`${outputLocations.production}/**/interface.js.map`,
		`${outputLocations.production}/**/*.interface.js`,
		`${outputLocations.production}/**/*.interface.js.map`,
	]);
}

// Moves required files into the production output folder
function prodPack()
{
	return src([
		'src/assets/**/*',
		'package*.json',
		'LICENSE.md',
	], { base: '.' })
		.pipe(rename({ dirname: '' }))
		.pipe(
			dest(outputLocations.production),
		);
}

// Uses nodemon to monitor the .ts files for changes
// When it detects a change, it runs the compiler and restarts the app
function devMon(done: (error?: Error | null | undefined) => void)
{
	return nodemon({
		script: `${outputLocations.development}/index.js`,
		delay: nodemonChangeDelay,
		watch: ['src'],
		ext: 'ts',
		nodeArgs: ['--inspect=3001', '-r', 'source-map-support/register'],
		env: {
			...process.env,
			/* eslint-disable @typescript-eslint/naming-convention */
			NODE_ENV: 'development',
			/* eslint-enable @typescript-eslint/naming-convention */
		},
		tasks: [
			'build:dev',
		],
		done,
		verbose: true,
	});
}

// Packs the files into a zip to be created into a release
function releasePack()
{
	return src(`${outputLocations.production}/**/*`)
		.pipe(zip('release.zip'))
		.pipe(dest(__dirname));
}

// Build for development task
const buildDev = series(
	devCompile,

	devClean,
);

// Build for production task
const buildProd = series(
	prodEmpty,
	parallel(
		prodCompile,
		prodPack,
	),

	prodClean,
);

// Handle the pre-release
const releasePre = series(
	releasePack,
);

// Handle file changes in development
const watch = series(
	devEmpty,
	buildDev,
	devMon,
);

task('build:dev', buildDev);
task('build:prod', buildProd);
task('release:pre', releasePre);
task('watch', watch);

export default buildProd;
