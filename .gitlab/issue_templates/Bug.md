# **Bug Report**

## **Description**

A clear and concise description of what the bug is

## **To Reproduce**

Steps to reproduce the bug

1. Go to ...
2. Click on ...
3. Scroll down to ...
4. etc ...

## **Expected Behaviour**

A clear and concise description of what you expected to happen

## **Logs or Screenshots**

If applicable, add logs or screenshots to help explain your problem

## **System**

- **Operating System:** [e.g. Windows 10, OSX etc.]
- **Node Version:** [e.g. v16.6.1, obtained by running `node -v` in the terminal]
- **Bot Version:** [e.g. 5.0.0]

## **Additional Information**

Add any other information about the problem here

/label ~bug
