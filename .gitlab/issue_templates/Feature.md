# **Feature**

## **Description**

A clear and concise description of the feature

## **Ideal Solution**

A clear and concise description of what you think the ideal solution would be

## **Alternative Solutions**

If applicable, describe other solutions you think could work

## **Screenshots**

If applicable, add screenshots to help explain your request

## **Additional Information**

Add any other information about the request here

/label ~feature
